﻿using Autofac;
using Zombus.DependencyResolution;

// ReSharper disable CheckNamespace

namespace Zombus.Configuration
// ReSharper restore CheckNamespace
{
    public static class AutofacBusBuilderConfigurationExtensions
    {
        public static BusBuilderConfiguration WithAutofacDefaults(this BusBuilderConfiguration configuration, IComponentContext componentContext)
        {
            return configuration
                .WithDependencyResolver(componentContext.Resolve<IDependencyResolver>())
                .WithLogger(componentContext.Resolve<ILogger>());
        }
    }
}