﻿using Autofac;

using Zombus.Autofac.Infrastructure;
using Zombus.DependencyResolution;
using Zombus.Extensions;

// ReSharper disable CheckNamespace

namespace Zombus.Configuration
// ReSharper restore CheckNamespace
{
    public static class ZombusContainerBuilderExtensions
    {
        public static ContainerBuilder RegisterZombus(this ContainerBuilder builder, ITypeProvider typeProvider)
        {
            builder.RegisterInstance(typeProvider)
                   .As<ITypeProvider>()
                   .SingleInstance();

            builder.RegisterType<AutofacDependencyResolver>()
                   .As<IDependencyResolver>()
                   .SingleInstance();

            typeProvider.AllResolvableTypes().Do(t => builder.RegisterType(t)
                                        .AsSelf()
                                        .InstancePerLifetimeScope())
                        .Done();

            return builder;
        }
    }
}