using System;
using Zombus.LargeMessages.Azure.Extensions;
using Zombus.LargeMessages.Azure.Configuration.Settings;

namespace Zombus.LargeMessages.Azure.Infrastructure.Http
{
    internal class UriFormatter : IUriFormatter
    {
        private readonly AzureBlobStorageContainerSharedAccessSignatureSetting _containerSharedAccessSignatureSetting;
        private readonly AzureBlobStorageContainerUriSetting _containerUriSetting;

        public UriFormatter(AzureBlobStorageContainerUriSetting containerUriSetting, AzureBlobStorageContainerSharedAccessSignatureSetting containerSharedAccessSignatureSetting)
        {
            _containerUriSetting = containerUriSetting;
            _containerSharedAccessSignatureSetting = containerSharedAccessSignatureSetting;
        }

        public Uri FormatUri(string storageKey)
        {
            return new Uri(_containerUriSetting.Value.Append(storageKey) + _containerSharedAccessSignatureSetting, UriKind.Absolute);
        }

    }
}