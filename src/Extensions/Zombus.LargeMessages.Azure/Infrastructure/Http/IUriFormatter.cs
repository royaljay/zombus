using System;

namespace Zombus.LargeMessages.Azure.Infrastructure.Http
{
    internal interface IUriFormatter
    {
        Uri FormatUri(string storageKey);
    }
}