using Zombus.Configuration.Settings;

namespace Zombus.LargeMessages.Azure.Configuration.Settings
{
    public class AzureBlobStorageContainerSharedAccessSignatureSetting : Setting<string> { }
}