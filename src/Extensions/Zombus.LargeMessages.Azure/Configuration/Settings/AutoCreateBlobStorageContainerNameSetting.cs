using Zombus.Configuration.Settings;

namespace Zombus.LargeMessages.Azure.Configuration.Settings
{
    public class AutoCreateBlobStorageContainerNameSetting : Setting<string>
    {
        public override string Default
        {
            get { return "messagebodies"; }
        }
    }
}