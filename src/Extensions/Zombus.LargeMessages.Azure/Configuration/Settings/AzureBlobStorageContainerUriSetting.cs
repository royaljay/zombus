using System;
using Zombus.Configuration.Settings;

namespace Zombus.LargeMessages.Azure.Configuration.Settings
{
    public class AzureBlobStorageContainerUriSetting : Setting<Uri> { }
}