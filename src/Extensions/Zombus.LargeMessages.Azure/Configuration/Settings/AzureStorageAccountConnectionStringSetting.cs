using Zombus.Configuration.Settings;

namespace Zombus.LargeMessages.Azure.Configuration.Settings
{
    public class AzureStorageAccountConnectionStringSetting : Setting<string>
    {
    }
}