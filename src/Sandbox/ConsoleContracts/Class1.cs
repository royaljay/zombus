﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zombus.MessageContracts;

namespace ConsoleContracts
{
    public class DoStuff : IBusCommand
    {
        public string Stuff { get; set; }
    }

    public class NotifyStuff : IBusEvent
    {
        public string Message { get; set; }
    }
}
