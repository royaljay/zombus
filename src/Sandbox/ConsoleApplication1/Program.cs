﻿using ConsoleContracts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Zombus;
using Zombus.Configuration;
using Zombus.Infrastructure;
using Zombus.Infrastructure.DependencyResolution;
using Zombus.Serializers.Json;

namespace ConsoleApplication1
{
    public class Program
    {
        public static Stopwatch Timer;

        private static void Main(string[] args)
        {

            Timer = new Stopwatch();

            Timer.Start();
            const string connectionString =
                "Endpoint=sb://kno2-jferguson.servicebus.windows.net/;SharedAccessKeyName=Kno2;SharedAccessKey=YllaqQuGYa6WjZgMvCG8IlqC7IkDe49kCGkiUZQpZYs=";

            var typeProvider = new AssemblyScanningTypeProvider(Assembly.GetExecutingAssembly(), typeof(DoStuff).Assembly);
            var b = new BusBuilder()
                .Configure()
                .WithConnectionString(connectionString)
                .WithTypesFrom(typeProvider)
            

                .WithDependencyResolver(new DependencyResolver(typeProvider))
                .WithNames("Ninja", Environment.MachineName)
                .WithSerializer(new JsonSerializer())
                .WithDefaultTimeout(TimeSpan.FromSeconds(10))
                .Build();

            
          // b.Start().Wait();
            Console.WriteLine(Timer.ElapsedMilliseconds);


            Task.Run(
                async () =>
                      {
                          var doStuffs = new List<DoStuff>();
                          for (int i = 0; i < 20; i++)
                          {
                              doStuffs.Add(new DoStuff {Stuff = "Yola ola"});
                          }

                          await b.SendAll(doStuffs);
                      }
                );


            Task.Run(
                async () =>
                      {

                          for (int i = 0; i < 25; i++)
                          {

                              await b.PublishAsync(new NotifyStuff {Message = "Get on up"});
                              await b.PublishAsync(new NotifyStuff {Message = "Get on up 2"});
                          }

                      }
                );

            Console.WriteLine("Running..");
            Console.ReadKey();
        }
    }

   
}

