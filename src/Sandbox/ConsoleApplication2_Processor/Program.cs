﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using ConsoleContracts;
using Zombus;
using Zombus.Configuration;
using Zombus.Handlers;
using Zombus.Infrastructure;
using Zombus.Infrastructure.DependencyResolution;
using Zombus.MessageContracts;
using Zombus.Serializers.Json;

namespace ConsoleApplication2_Processor
{
    public class Program
    {
        public static Stopwatch Timer;
        static void Main(string[] args)
        {
            Timer = new Stopwatch();

            Timer.Start();
            const string connectionString = "Endpoint=sb://kno2-jferguson.servicebus.windows.net/;SharedAccessKeyName=Kno2;SharedAccessKey=YllaqQuGYa6WjZgMvCG8IlqC7IkDe49kCGkiUZQpZYs=";

            var typeProvider = new AssemblyScanningTypeProvider(Assembly.GetExecutingAssembly(), typeof(DoStuff).Assembly);
            var b = new BusBuilder()
                        .Configure()
                        .WithConnectionString(connectionString)
                        .WithTypesFrom(typeProvider)
                        .WithConcurrentHandlerLimit(1)
                        .WithPrefetchCount(1)
                        .WithDependencyResolver(new DependencyResolver(typeProvider))
                        .WithNames("Rockstar", Environment.MachineName)
                        .WithSerializer(new JsonSerializer())
                        .WithDefaultTimeout(TimeSpan.FromSeconds(10))
                        .Build();


            b.Start().Wait();
            Console.WriteLine(Timer.ElapsedMilliseconds);
           
            Console.WriteLine("Running..");
            Console.ReadKey();
        }
    }

    public class NotifyStuffEventHandler : IHandleMulticastEvent<NotifyStuff>
    {
        public async Task Handle(NotifyStuff busEvent)
        {
            await Task.Run(() => Console.WriteLine("{0} of {1}, Time {2} for {3}", Thread.CurrentThread.ManagedThreadId,
               Process.GetCurrentProcess().Threads.Count, Program.Timer.ElapsedMilliseconds, GetType()));
        }
    }

    public class NotifyStuffEvent2Handler : IHandleMulticastEvent<NotifyStuff>
    {
        public async Task Handle(NotifyStuff busEvent)
        {
            await Task.Run(() => Console.WriteLine("{0} of {1}, Time {2} for {3}", Thread.CurrentThread.ManagedThreadId,
               Process.GetCurrentProcess().Threads.Count, Program.Timer.ElapsedMilliseconds, GetType()));
        }
    }

    public class DoingStuffHandler : IHandleCommand<DoStuff>
    {
        public async Task Handle(DoStuff busCommand)
        {
            //throw new Exception("BOOM!@");
            await Task.Run(() => Console.WriteLine("{0} of {1}, Time {2}", Thread.CurrentThread.ManagedThreadId,
                Process.GetCurrentProcess().Threads.Count, Program.Timer.ElapsedMilliseconds));
        }
    }
}
