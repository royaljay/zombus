using System.Threading.Tasks;

namespace Zombus.Configuration
{
    internal interface IMessageReceiverManager
    {
        Task SubscribeAllReceivers(MessageReceiverTypes messageReceiverTypes);
        Task Stop(MessageReceiverTypes messageReceiverTypes);
    }
}