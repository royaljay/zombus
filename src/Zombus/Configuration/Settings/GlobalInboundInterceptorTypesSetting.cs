﻿using System;

namespace Zombus.Configuration.Settings
{
    public class GlobalInboundInterceptorTypesSetting : Setting<Type[]>
    {
        public override Type[] Default
        {
            get { return new Type[0]; }
        }
    }
}