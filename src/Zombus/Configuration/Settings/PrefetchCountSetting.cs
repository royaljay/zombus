﻿using System;
using System.Collections.Generic;

namespace Zombus.Configuration.Settings
{
    public class PrefetchCountSetting : Setting<int>
    {  
        public override int Default
        {
            get { return Environment.ProcessorCount * 2; }
        }
    }
}