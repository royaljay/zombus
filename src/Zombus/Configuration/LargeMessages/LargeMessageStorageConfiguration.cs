﻿using Zombus.Configuration.LargeMessages.Settings;

namespace Zombus.Configuration.LargeMessages
{
    public class LargeMessageStorageConfiguration : IZombusConfiguration
    {
        internal ILargeMessageBodyStore LargeMessageBodyStore { get; set; }
        internal MaxSmallMessageSizeSetting MaxSmallMessageSize { get; set; }
        internal MaxLargeMessageSizeSetting MaxLargeMessageSize { get; set; }
    }
}