using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zombus.Extensions;
using Zombus.Infrastructure;

namespace Zombus.Configuration
{
    internal class MessageRecevierManager : IMessageReceiverManager
    {
        private readonly IEventBasedReceiver[] _commandEventBasedReceivers;
        private readonly IEventBasedReceiver[] _multicastEventEventBasedReceivers;
        private readonly IEventBasedReceiver[] _competingEventEventBasedReceivers;

        public MessageRecevierManager(IEnumerable<IEventBasedReceiver> commandMessagerReceivers,
                                   IEnumerable<IEventBasedReceiver> multicastEventMessageReceivers,
                                   IEnumerable<IEventBasedReceiver> competingEventMessageReceivers)
        {
         
            _commandEventBasedReceivers = commandMessagerReceivers.ToArray();
            _multicastEventEventBasedReceivers = multicastEventMessageReceivers.ToArray();
            _competingEventEventBasedReceivers = competingEventMessageReceivers.ToArray();
        }

        public async Task SubscribeAllReceivers(MessageReceiverTypes messageReceiverTypes)
        {
            await DoForAllReceivers(messageReceiverTypes, receiver => receiver.SubscribeToQueue());
        }

        public async Task Stop(MessageReceiverTypes messageReceiverTypes)
        {
            //we dont stop
            //await DoForAllReceivers(messageReceiverTypes, receiver => receiver.UnSubscribeToQueue());
            await Task.FromResult<object>(null);
        }

        private async Task DoForAllReceivers(MessageReceiverTypes waitForReceiverTypes, Func<IEventBasedReceiver, Task> action)
        {
            var typesToProcessInBackground = (MessageReceiverTypes)((int)waitForReceiverTypes ^ -1);

              var messageReceiversToWaitFor = GetMessageReceiver(waitForReceiverTypes).ToArray();
              var messageReceiversToHandleInBackground = GetMessageReceiver(typesToProcessInBackground).ToArray();

              await messageReceiversToWaitFor.Select(async (receiver) =>  await action(receiver)).WhenAll();
              await messageReceiversToHandleInBackground.Select(async (receiver) => await action(receiver)).WhenAll();
        }

        private IEnumerable<IEventBasedReceiver> GetMessageReceiver(MessageReceiverTypes messageReceiverTypes)
        {
            if (messageReceiverTypes.HasFlag(MessageReceiverTypes.Command)) foreach (var messageReceiver in _commandEventBasedReceivers) yield return messageReceiver;
            if (messageReceiverTypes.HasFlag(MessageReceiverTypes.MulticastEvent)) foreach (var messageReceiver in _multicastEventEventBasedReceivers) yield return messageReceiver;
            if (messageReceiverTypes.HasFlag(MessageReceiverTypes.CompetingEvent)) foreach (var messageReceiver in _competingEventEventBasedReceivers) yield return messageReceiver;
        }
    }
}