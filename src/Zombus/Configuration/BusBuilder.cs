﻿using System;
using System.Reflection;
using System.ServiceModel.Configuration;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Zombus.ConcurrentCollections;
using Zombus.Configuration.Settings;
using Zombus.Infrastructure;
using Zombus.Infrastructure.Commands;
using Zombus.Infrastructure.Events;
using Zombus.Infrastructure.Heartbeat;
using Zombus.Infrastructure.PropertyInjection;
using Zombus.PoisonMessages;
using MessagingFactory = Microsoft.ServiceBus.Messaging.MessagingFactory;

namespace Zombus.Configuration
{
    public class BusBuilder
    {
        public BusBuilderConfiguration Configure()
        {
            return new BusBuilderConfiguration();
        }

        internal static Bus Build(BusBuilderConfiguration configuration)
        {
            var logger = configuration.Logger;
            logger.Debug("Constructing bus...");

            var container = new PoorMansIoC();

            RegisterPropertiesFromConfigurationObject(container, configuration);
            RegisterPropertiesFromConfigurationObject(container, configuration.LargeMessageStorageConfiguration);
            RegisterPropertiesFromConfigurationObject(container, configuration.Debugging);

            container.Register<Func<NamespaceManager>> (() =>
            {
                var ns = NamespaceManager.CreateFromConnectionString(container.Resolve<ConnectionStringSetting>());
                ns.Settings.OperationTimeout = configuration.DefaultTimeout;
              
                return ns;
            });
        
            container.Register<Func<MessagingFactory>>(() =>
            {
                var messagingFactory = MessagingFactory.CreateFromConnectionString(container.Resolve<ConnectionStringSetting>());
                messagingFactory.PrefetchCount = container.Resolve<ConcurrentHandlerLimitSetting>();
                return messagingFactory;
            });
   

            if (configuration.Debugging.RemoveAllExistingNamespaceElements)
            {
                var namespaceCleanser = container.Resolve<NamespaceCleanser>();
                namespaceCleanser.RemoveAllExistingNamespaceElements().Wait();
            }

            logger.Debug("Creating message receivers...");

            var messageReceiverManager = new MessageRecevierManager(
                container.Resolve<CommandMessageReceiverFactory>().CreateAll(),
                container.Resolve<MulticastEventMessageReceiverFactory>().CreateAll(),
                container.Resolve<CompetingEventMessageReceiverFactory>().CreateAll());

            logger.Debug("Message receivers are all created.");

            var bus = new Bus(container.Resolve<ILogger>(),
                              container.Resolve<ICommandSender>(),
                              container.Resolve<IEventSender>(),
                              messageReceiverManager,
                              container.Resolve<DeadLetterQueues>(),
                              container.Resolve<IHeartbeat>());

            bus.Starting += delegate
                            {
                                container.Resolve<AzureQueueManager>().WarmUp();
                                container.Resolve<PropertyInjector>().Bus = bus;
                            };

            bus.Disposing += delegate { container.Dispose(); };

            logger.Info("Bus built. Job done!");

            return bus;
        }

        private static void RegisterPropertiesFromConfigurationObject(PoorMansIoC container, object configuration)
        {
            foreach (var prop in configuration.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                var value = prop.GetValue(configuration);
                if (value == null) continue;

                container.Register(value);
            }
        }
    }
}