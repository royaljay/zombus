﻿namespace Zombus.Configuration.Debug
{
    public class BusBuilderDebuggingConfiguration: IZombusConfiguration
    {
        internal bool RemoveAllExistingNamespaceElements { get; set; }
        internal bool UseInProcessBus { get; set; }
    }
}