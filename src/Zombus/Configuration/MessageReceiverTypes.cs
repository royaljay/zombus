using System;

namespace Zombus.Configuration
{
    [Flags]
    public enum MessageReceiverTypes
    {
        Default = All,

        None = 0,
        All = -1,

        Command = 1 << 0,
        MulticastEvent = 1 << 1,
        CompetingEvent = 1 << 2
    }
}