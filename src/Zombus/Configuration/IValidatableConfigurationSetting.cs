﻿using System.Collections.Generic;

namespace Zombus.Configuration
{
    internal interface IValidatableConfigurationSetting
    {
        IEnumerable<string> Validate();
    }
}