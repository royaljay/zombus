﻿using System;
using System.Threading.Tasks;
using Zombus.Infrastructure;
using Zombus.MessageContracts;

namespace Zombus
{
    public static class DelayedSendingExtensions
    {
        private static IClock _clock = new SystemClock();

        internal static void SetClockStrategy(IClock clock)
        {
            _clock = clock;
        }

        public static Task SendAfter<TBusCommand>(this IBus bus, TBusCommand busCommand, TimeSpan delay) where TBusCommand : IBusCommand
        {
            var deliveryTime = _clock.UtcNow.Add(delay);
            return bus.SendAtAsync(busCommand, deliveryTime);
        }
    }
}