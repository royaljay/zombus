﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zombus.MessageContracts;

namespace Zombus
{
    public static class BatchSendingExtensions
    {
        public static Task SendAll(this IBus bus, IEnumerable<IBusCommand> busCommands)
        {
            return Task.Run(async () => { await Task.WhenAll(busCommands.Select(e => bus.SendAsync(e))); });
        }

        public static Task PublishAll(this IBus bus, IEnumerable<IBusEvent> busEvents)
        {
            return Task.Run(async () => { await Task.WhenAll(busEvents.Select(e => bus.PublishAsync(e))); });
        }
    }
}