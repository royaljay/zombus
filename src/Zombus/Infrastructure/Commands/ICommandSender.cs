﻿using System;
using System.Threading.Tasks;
using Zombus.MessageContracts;

namespace Zombus.Infrastructure.Commands
{
    internal interface ICommandSender
    {
        Task SendAsync<TBusCommand>(TBusCommand busCommand) where TBusCommand : IBusCommand;
        Task SendAsyncAt<TBusCommand>(TBusCommand busCommand, DateTimeOffset whenToSend) where TBusCommand : IBusCommand;
    }
}