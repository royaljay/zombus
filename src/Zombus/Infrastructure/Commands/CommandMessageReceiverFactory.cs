﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.ServiceBus;
using Zombus.Extensions;
using Zombus.Configuration;
using Zombus.Handlers;
using Zombus.Routing;

namespace Zombus.Infrastructure.Commands
{
    internal class CommandMessageReceiverFactory : ICreateComponents
    {
        private readonly ILogger _logger;
        private readonly IHandlerMapper _handlerMapper;
        private readonly IMessagingFactory _messagingFactory;
        private readonly IRouter _router;
        private readonly ITypeProvider _typeProvider;
        private readonly IMessageDispatcherFactory _messageDispatcherFactory;

        private readonly GarbageMan _garbageMan = new GarbageMan();

        public CommandMessageReceiverFactory(IHandlerMapper handlerMapper,
                                          ILogger logger,
                                          IMessageDispatcherFactory messageDispatcherFactory,
                                          IMessagingFactory messagingFactory,
                                          IRouter router,
                                          ITypeProvider typeProvider)
        {
            _handlerMapper = handlerMapper;
            _logger = logger;
            _messagingFactory = messagingFactory;
            _router = router;
            _typeProvider = typeProvider;
            _messageDispatcherFactory = messageDispatcherFactory;
        }

        public IEnumerable<IEventBasedReceiver> CreateAll()
        {
            var openGenericHandlerType = typeof (IHandleCommand<>);
            var handlerTypes = _typeProvider.CommandHandlerTypes.ToArray();

            // Create a single connection to each command queue determined by routing
            var allMessageTypesHandledByThisEndpoint = _handlerMapper.GetMessageTypesHandledBy(openGenericHandlerType, handlerTypes);
            var bindings = allMessageTypesHandledByThisEndpoint
                .Select(m => new {MessageType = m, QueuePath = _router.Route(m, QueueOrTopic.Queue)})
                .GroupBy(b => b.QueuePath)
                .Select(g => new {QueuePath = g.Key, HandlerTypes = g.SelectMany(x => _handlerMapper.GetHandlerTypesFor(openGenericHandlerType, x.MessageType))});

            // Each binding to a queue can handle one or more command types depending on the routes that are defined
            foreach (var binding in bindings)
            {
                var messageTypes = _handlerMapper.GetMessageTypesHandledBy(openGenericHandlerType, binding.HandlerTypes).ToArray();
                var handlerMap = _handlerMapper.GetHandlerMapFor(openGenericHandlerType, messageTypes);
                var dispatcher = _messageDispatcherFactory.Create(openGenericHandlerType, handlerMap);

                _logger.Debug("Creating message receiver for command queue '{0}' handling {1}", binding.QueuePath, messageTypes.ToTypeNameSummary(selector: t => t.Name));
               
                var messageReceiver = _messagingFactory.GetQueueReceiver(binding.QueuePath);
                messageReceiver.RegisterDispatcher(dispatcher);
                
                _garbageMan.Add(messageReceiver);

                yield return messageReceiver;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing) return;
            _garbageMan.Dispose();
        }
    }
}