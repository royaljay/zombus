using Microsoft.ServiceBus.Messaging;

namespace Zombus.Infrastructure.PropertyInjection
{
    internal interface IPropertyInjector
    {
        void Inject(object handlerOrInterceptor, BrokeredMessage brokeredMessage);
    }
}