using System;

namespace Zombus.Infrastructure.Dispatching
{
    internal interface IDispatchContextManager
    {
        IDispatchContext GetCurrentDispatchContext();
        IDisposable StartNewDispatchContext(IDispatchContext dispatchContext);
    }
}