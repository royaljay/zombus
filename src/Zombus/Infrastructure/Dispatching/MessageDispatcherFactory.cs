﻿using System;
using System.Collections.Generic;
using Zombus.Extensions;
using Zombus.Configuration.Settings;
using Zombus.DependencyResolution;
using Zombus.Handlers;
using Zombus.Infrastructure.Commands;
using Zombus.Infrastructure.Events;
using Zombus.Infrastructure.PropertyInjection;
using Zombus.Interceptors.Inbound;
using Zombus.Interceptors.Outbound;

namespace Zombus.Infrastructure
{
    internal class MessageDispatcherFactory : IMessageDispatcherFactory
    {
        private readonly IBrokeredMessageFactory _brokeredMessageFactory;
        private readonly IClock _clock;
        private readonly IDependencyResolver _dependencyResolver;
        private readonly IInboundInterceptorFactory _inboundInterceptorFactory;
        private readonly IOutboundInterceptorFactory _outboundInterceptorFactory;
        private readonly ILogger _logger;
        private readonly IMessagingFactory _messagingFactory;
        private readonly DefaultMessageLockDurationSetting _defaultMessageLockDuration;
        private readonly IPropertyInjector _propertyInjector;

        public MessageDispatcherFactory(DefaultMessageLockDurationSetting defaultMessageLockDuration,
                                        IBrokeredMessageFactory brokeredMessageFactory,
                                        IClock clock,
                                        IDependencyResolver dependencyResolver,
                                        IInboundInterceptorFactory inboundInterceptorFactory,
                                        ILogger logger,
                                        IMessagingFactory messagingFactory,
                                        IOutboundInterceptorFactory outboundInterceptorFactory,
                                        IPropertyInjector propertyInjector)
        {
            _brokeredMessageFactory = brokeredMessageFactory;
            _clock = clock;
            _dependencyResolver = dependencyResolver;
            _inboundInterceptorFactory = inboundInterceptorFactory;
            _logger = logger;
            _messagingFactory = messagingFactory;
            _outboundInterceptorFactory = outboundInterceptorFactory;
            _defaultMessageLockDuration = defaultMessageLockDuration;
            _propertyInjector = propertyInjector;
        }

        public IMessageDispatcher Create(Type openGenericHandlerType, IReadOnlyDictionary<Type, Type[]> handlerMap)
        {
            return BuildDispatcher(openGenericHandlerType, handlerMap);
        }

        private IMessageDispatcher BuildDispatcher(Type openGenericHandlerType, IReadOnlyDictionary<Type, Type[]> handlerMap)
        {
            if (openGenericHandlerType == typeof (IHandleCommand<>))
            {
                return new CommandMessageDispatcher(_brokeredMessageFactory,
                                                    _clock,
                                                    _dependencyResolver,
                                                    _inboundInterceptorFactory,
                                                    _logger,
                                                    handlerMap,
                                                    _defaultMessageLockDuration,
                                                    _propertyInjector);
            }

            if (openGenericHandlerType == typeof (IHandleCompetingEvent<>))
            {
                return new CompetingEventMessageDispatcher(_brokeredMessageFactory,
                                                           _clock,
                                                           _dependencyResolver,
                                                           _inboundInterceptorFactory,
                                                           handlerMap,
                                                           _defaultMessageLockDuration,
                                                           _propertyInjector,
                                                           _logger);
            }

            if (openGenericHandlerType == typeof (IHandleMulticastEvent<>))
            {
                return new MulticastEventMessageDispatcher(_brokeredMessageFactory,
                                                           _clock,
                                                           _dependencyResolver,
                                                           _inboundInterceptorFactory,
                                                           handlerMap,
                                                           _defaultMessageLockDuration,
                                                           _propertyInjector,
                                                           _logger);
            }
        

            throw new NotSupportedException("There is no dispatcher for the handler type {0}.".FormatWith(openGenericHandlerType.FullName));
        }
    }
}