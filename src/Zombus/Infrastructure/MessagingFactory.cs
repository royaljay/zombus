﻿using System;
using System.Collections.Concurrent;
using Zombus.Extensions;
using Zombus.ConcurrentCollections;
using Zombus.Configuration;
using Zombus.Configuration.Settings;
using Zombus.Infrastructure.MessageSendersAndReceivers;

namespace Zombus.Infrastructure
{
    internal class MessagingFactory : IMessagingFactory, ICreateComponents
    {
        private readonly IQueueManager _queueManager;

        private readonly ThreadSafeDictionary<string, IMessageSender> _queueMessageSenders = new ThreadSafeDictionary<string, IMessageSender>();
        private readonly ThreadSafeDictionary<string, IEventBasedReceiver> _queueMessageReceivers = new ThreadSafeDictionary<string, IEventBasedReceiver>();
        private readonly ThreadSafeDictionary<string, IMessageSender> _topicMessageSenders = new ThreadSafeDictionary<string, IMessageSender>();
        private readonly ThreadSafeDictionary<string, IEventBasedReceiver> _topicMessageReceivers = new ThreadSafeDictionary<string, IEventBasedReceiver>();
        private readonly GarbageMan _garbageMan = new GarbageMan();
        private readonly ConcurrentHandlerLimitSetting _concurrentHandlerLimit;
        private readonly ILogger _logger;
        private readonly PrefetchCountSetting _prefetchCount;

        public MessagingFactory(ConcurrentHandlerLimitSetting concurrentHandlerLimit, ILogger logger, PrefetchCountSetting prefetchCount, IQueueManager queueManager)
        {
            _queueManager = queueManager;
            _concurrentHandlerLimit = concurrentHandlerLimit;
            _logger = logger;
            _prefetchCount = prefetchCount;
        }

        public IMessageSender GetQueueSender(string queuePath)
        {
            return _queueMessageSenders.GetOrAdd(queuePath, CreateQueueSender);
        }

        public IEventBasedReceiver GetQueueReceiver(string queuePath)
        {
            return _queueMessageReceivers.GetOrAdd(queuePath, CreateQueueReceiver);
        }

        public IMessageSender GetTopicSender(string topicPath)
        {
            return _topicMessageSenders.GetOrAdd(topicPath, CreateTopicSender);
        }

        public IEventBasedReceiver GetTopicReceiver(string topicPath, string subscriptionName)
        {
            var key = "{0}/{1}".FormatWith(topicPath, subscriptionName);
            return _topicMessageReceivers.GetOrAdd(key, k => CreateTopicReceiver(topicPath, subscriptionName));
        }

        private IMessageSender CreateQueueSender(string queuePath)
        {
            var sender = new ZombusQueueMessageSender(_queueManager, queuePath, _logger);
            _garbageMan.Add(sender);
            return sender;
        }

        private IEventBasedReceiver CreateQueueReceiver(string queuePath)
        {
            var receiver = new OnReceiveMessageReceiver(_queueManager, queuePath, _concurrentHandlerLimit, _prefetchCount, _logger);
            _garbageMan.Add(receiver);
            return receiver;
        }

        private IMessageSender CreateTopicSender(string topicPath)
        {
            var sender = new ZombusTopicMessageSender(_queueManager, topicPath, _logger);
            _garbageMan.Add(sender);
            return sender;
        }

        private IEventBasedReceiver CreateTopicReceiver(string topicPath, string subscriptionName)
        {
            var receiver = new OnReceiveSubscriptionMessageReceiver(_queueManager, topicPath, subscriptionName, _concurrentHandlerLimit, _prefetchCount, _logger);
            _garbageMan.Add(receiver);
            return receiver;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing) return;
            _garbageMan.Dispose();
        }
    }
}