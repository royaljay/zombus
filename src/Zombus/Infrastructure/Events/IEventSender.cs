﻿using System.Threading.Tasks;
using Zombus.MessageContracts;

namespace Zombus.Infrastructure.Events
{
    internal interface IEventSender
    {
        Task PublishAsync<TBusEvent>(TBusEvent busEvent) where TBusEvent : IBusEvent;
    }
}