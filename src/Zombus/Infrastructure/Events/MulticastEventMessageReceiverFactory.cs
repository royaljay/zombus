﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zombus.Infrastructure.MessageSendersAndReceivers;
using Zombus.Configuration;
using Zombus.Configuration.Settings;
using Zombus.Handlers;
using Zombus.Routing;

namespace Zombus.Infrastructure.Events
{
    internal class MulticastEventMessageReceiverFactory : ICreateComponents
    {
        private readonly ApplicationNameSetting _applicationName;
        private readonly InstanceNameSetting _instanceName;
        private readonly ILogger _logger;
        private readonly IMessageDispatcherFactory _messageDispatcherFactory;
        private readonly IClock _clock;
        private readonly IHandlerMapper _handlerMapper;
        private readonly ITypeProvider _typeProvider;
        private readonly IMessagingFactory _messagingFactory;
        private readonly IRouter _router;

        private readonly GarbageMan _garbageMan = new GarbageMan();

        internal MulticastEventMessageReceiverFactory(ApplicationNameSetting applicationName,
                                                   InstanceNameSetting instanceName,
                                                   IClock clock,
                                                   IHandlerMapper handlerMapper,
                                                   ILogger logger,
                                                   IMessageDispatcherFactory messageDispatcherFactory,
                                                   IMessagingFactory messagingFactory,
                                                   IRouter router,
                                                   ITypeProvider typeProvider)
        {
            _applicationName = applicationName;
            _instanceName = instanceName;
            _clock = clock;
            _handlerMapper = handlerMapper;
            _logger = logger;
            _messageDispatcherFactory = messageDispatcherFactory;
            _messagingFactory = messagingFactory;
            _router = router;
            _typeProvider = typeProvider;
        }

        public IEnumerable<IEventBasedReceiver> CreateAll()
        {
            var openGenericHandlerType = typeof(IHandleMulticastEvent<>);
            var handlerTypes = _typeProvider.MulticastEventHandlerTypes.ToArray();

            // Events are routed to Topics and we'll create a subscription per instance of the logical endpoint to enable multicast behaviour
            var allMessageTypesHandledByThisEndpoint = _handlerMapper.GetMessageTypesHandledBy(openGenericHandlerType, handlerTypes);
            var bindings = allMessageTypesHandledByThisEndpoint
                .Select(m => new { MessageType = m, TopicPath = _router.Route(m, QueueOrTopic.Topic) })
                .GroupBy(b => b.TopicPath)
                .Select(g => new
                             {
                                 TopicPath = g.Key,
                                 MessageTypes = g.Select(x => x.MessageType),
                                 HandlerTypes = g.SelectMany(x => _handlerMapper.GetHandlerTypesFor(openGenericHandlerType, x.MessageType))
                             })
                .ToArray();

            if (bindings.Any(b => b.MessageTypes.Count() > 1))
                throw new NotSupportedException("Routing multiple message types through a single Topic is not supported.");

            foreach (var binding in bindings)
            {
                foreach (var handlerType in binding.HandlerTypes)
                {
                    var messageType = binding.MessageTypes.Single();
                    var subscriptionName = PathFactory.SubscriptionNameFor(_applicationName, _instanceName, handlerType);
                    var handlerMap = new Dictionary<Type, Type[]> { { messageType, new[] { handlerType } } };
                    var messageDispatcher =_messageDispatcherFactory.Create(openGenericHandlerType, handlerMap);


                    _logger.Debug("Creating message pump for multicast event subscription '{0}/{1}' handling {2}", binding.TopicPath, subscriptionName, messageType);
                    var messageReceiver = _messagingFactory.GetTopicReceiver(binding.TopicPath, subscriptionName);
                    messageReceiver.RegisterDispatcher(messageDispatcher);
                    _garbageMan.Add(messageReceiver);

                    yield return messageReceiver;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing) return;
            _garbageMan.Dispose();
        }
    }
}