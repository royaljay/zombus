﻿using System;

namespace Zombus.Infrastructure
{
    internal interface IClock
    {
        DateTimeOffset UtcNow { get; }
    }
}