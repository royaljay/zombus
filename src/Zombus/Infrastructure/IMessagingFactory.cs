﻿using Microsoft.ServiceBus.Messaging;
using Zombus.Infrastructure.MessageSendersAndReceivers;

namespace Zombus.Infrastructure
{
    internal interface IMessagingFactory
    {
        IMessageSender GetQueueSender(string queuePath);
        IEventBasedReceiver GetQueueReceiver(string queuePath);

        IMessageSender GetTopicSender(string topicPath);
        IEventBasedReceiver GetTopicReceiver(string topicPath, string subscriptionName);
    }
}