﻿using System;
using System.Linq;
using Microsoft.ServiceBus.Messaging;
using Zombus.Extensions;
using Zombus.Annotations;

namespace Zombus.Infrastructure.Logging
{
    internal static class MessageLoggingExtensions
    {
        internal static void LogDispatchAction(this ILogger logger, string dispatchAction, string queueOrTopicPath, BrokeredMessage message)
        {
            //("{DispatchAction} {ShortMessageTypeName} ({MessageId}) to {QueueOrTopicPath} ({@MessageMetadata})"
            var metadata = MessageMetadata.Create(message);
            logger.Debug("Debug : {0} {1} ({2}) to {3}",
                         dispatchAction,
                         metadata.ShortMessageTypeName,
                         metadata.MessageId,
                         queueOrTopicPath);
        }

        internal static void LogDispatchError(this ILogger logger, string dispatchAction, string queueOrTopicPath, BrokeredMessage message, Exception exception)
        {
            //"Error {DispatchAction} {ShortMessageTypeName} ({MessageId}) to {QueueOrTopicPath} ({@MessageMetadata})",
            var metadata = MessageMetadata.Create(message);
            logger.Error(exception,
                         "Error : {0} {1} ({2}) to {3}",
                         dispatchAction,
                         metadata.ShortMessageTypeName,
                         metadata.MessageId,
                         queueOrTopicPath);
        }

        private class MessageMetadata
        {
            private readonly Guid _messageId;
            private readonly Guid _correlationId;
            private readonly string _shortMessageTypeName;
            private readonly string _messageType;

            public static MessageMetadata Create(BrokeredMessage message)
            {
                var typeFullName = message.SafelyGetBodyTypeNameOrDefault();
                var shortMessageTypeName = typeFullName == null
                                   ? null
                                   : typeFullName.Split('.').Last();
                return new MessageMetadata(Guid.Parse(message.MessageId), Guid.Parse(message.CorrelationId), shortMessageTypeName, typeFullName);
            }

            private MessageMetadata(Guid messageId, Guid correlationId, string shortMessageTypeName, string messageType)
            {
                _messageId = messageId;
                _correlationId = correlationId;
                _shortMessageTypeName = shortMessageTypeName;
                _messageType = messageType;
            }

            [UsedImplicitly]
            public Guid MessageId
            {
                get { return _messageId; }
            }

            [UsedImplicitly]
            public Guid CorrelationId
            {
                get { return _correlationId; }
            }

            [UsedImplicitly]
            public string ShortMessageTypeName
            {
                get { return _shortMessageTypeName; }
            }

            [UsedImplicitly]
            public string MessageType
            {
                get { return _messageType; }
            }
        }
    }
}