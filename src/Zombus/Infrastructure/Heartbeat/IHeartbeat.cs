﻿using System.Threading.Tasks;

namespace Zombus.Infrastructure.Heartbeat
{
    internal interface IHeartbeat
    {
        Task Start();
        Task Stop();
    }
}