using System;
using System.Diagnostics;

namespace Zombus.Infrastructure.Heartbeat.PerformanceCounters.System
{
    internal class PerProcessCpuUsage : SystemPerformanceCounterWrapper
    {
        public PerProcessCpuUsage() : base(new PerformanceCounter("Process", "% Processor Time", "_Total"), v => v/Environment.ProcessorCount)
        {
        }
    }
}