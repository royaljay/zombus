using System;
using System.Diagnostics;

namespace Zombus.Infrastructure.Heartbeat.PerformanceCounters.System
{
    internal class TotalCpuUsage : SystemPerformanceCounterWrapper
    {
        public TotalCpuUsage() : base(new PerformanceCounter("Process", "% Processor Time", Process.GetCurrentProcess().ProcessName), v => v/Environment.ProcessorCount)
        {
        }
    }
}