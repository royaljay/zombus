using System.Diagnostics;

namespace Zombus.Infrastructure.Heartbeat.PerformanceCounters.System
{
    internal class PerProcessRamUsage : SystemPerformanceCounterWrapper
    {
        public PerProcessRamUsage() : base(new PerformanceCounter("Process", "Working Set", Process.GetCurrentProcess().ProcessName), v => v)
        {
        }
    }
}