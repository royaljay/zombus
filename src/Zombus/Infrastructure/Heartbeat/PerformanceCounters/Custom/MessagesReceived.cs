using Zombus.Infrastructure.MessageSendersAndReceivers;

namespace Zombus.Infrastructure.Heartbeat.PerformanceCounters.Custom
{
    internal class MessagesReceived : ZombusPerformanceCounterBase
    {
        public override long GetNextTransformedValue()
        {
            return GlobalMessageCounters.GetAndClearReceivedMessageCount();
        }
    }
}