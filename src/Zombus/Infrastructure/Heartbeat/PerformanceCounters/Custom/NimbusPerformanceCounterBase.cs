using System.Diagnostics;

namespace Zombus.Infrastructure.Heartbeat.PerformanceCounters.Custom
{
    internal abstract class ZombusPerformanceCounterBase : PerformanceCounterBase
    {
        public override string CategoryName
        {
            get { return "Zombus"; }
        }

        public override string CounterName
        {
            get { return GetType().Name; }
        }

        public override string InstanceName
        {
            get { return Process.GetCurrentProcess().ProcessName; }
        }
    }
}