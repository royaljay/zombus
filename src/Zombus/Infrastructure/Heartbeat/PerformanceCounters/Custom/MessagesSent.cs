using Zombus.Infrastructure.MessageSendersAndReceivers;

namespace Zombus.Infrastructure.Heartbeat.PerformanceCounters.Custom
{
    internal class MessagesSent : ZombusPerformanceCounterBase
    {
        public override long GetNextTransformedValue()
        {
            return GlobalMessageCounters.GetAndClearSentMessageCount();
        }
    }
}