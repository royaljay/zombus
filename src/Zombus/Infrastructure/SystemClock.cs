﻿using System;

namespace Zombus.Infrastructure
{
    internal class SystemClock : IClock
    {
        public DateTimeOffset UtcNow
        {
            get { return DateTimeOffset.UtcNow; }
        }
    }
}