﻿using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;

namespace Zombus.Infrastructure
{
    internal interface IMessageDispatcher
    {
        Task DispatchAsync(BrokeredMessage message);
    }
}