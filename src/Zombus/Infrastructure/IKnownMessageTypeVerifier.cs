using System;

namespace Zombus.Infrastructure
{
    internal interface IKnownMessageTypeVerifier
    {
        void AssertValidMessageType(Type messageType);
    }
}