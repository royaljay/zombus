using System;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;
using Zombus.Handlers;

namespace Zombus.Infrastructure.LongRunningTasks
{
    internal class LongRunningTaskWrapper<T> : LongRunningTaskWrapperBase
    {
        public LongRunningTaskWrapper(Task<T> handlerTask,
                                      ILongRunningTask longRunningHandler,
                                      BrokeredMessage message,
                                      IClock clock,
                                      ILogger logger,
                                      TimeSpan messageLockDuration)
            : base(handlerTask, longRunningHandler, message, clock, logger, messageLockDuration)
        {
        }

        public async Task<T> AwaitCompletion()
        {
            var firstTaskToComplete = await AwaitCompletionInternal(HandlerTask);
            return await ((Task<T>) firstTaskToComplete);
        }
    }

    internal class LongRunningTaskWrapper : LongRunningTaskWrapperBase
    {
        public LongRunningTaskWrapper(Task handlerTask,
                                      ILongRunningTask longRunningHandler,
                                      BrokeredMessage message,
                                      IClock clock,
                                      ILogger logger,
                                      TimeSpan messageLockDuration)
            : base(handlerTask, longRunningHandler, message, clock, logger, messageLockDuration)
        {
        }

        public async Task AwaitCompletion()
        {
            var firstTaskToComplete = await AwaitCompletionInternal(HandlerTask);
            await firstTaskToComplete;
        }
    }
}