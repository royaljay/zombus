﻿using System;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;

namespace Zombus.Infrastructure.MessageSendersAndReceivers
{
    internal interface IMessageSender : IDisposable
    {
        Task SendAsync(BrokeredMessage message);
    }
}