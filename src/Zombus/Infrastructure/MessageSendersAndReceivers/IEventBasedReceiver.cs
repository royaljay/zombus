﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Zombus.Infrastructure
{
    internal interface IEventBasedReceiver : IDisposable
    {
        Task SubscribeToQueue();
        void RegisterDispatcher(IMessageDispatcher dispatcher);
       
    }
}