using Microsoft.ServiceBus.Messaging;
using Zombus.Extensions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Zombus.Configuration.Settings;

namespace Zombus.Infrastructure.MessageSendersAndReceivers
{
    internal class OnReceiveSubscriptionMessageReceiver : IEventBasedReceiver
    {
        private readonly IQueueManager _queueManager;
        private readonly string _topicPath;
        private readonly string _subscriptionName;
        private readonly ConcurrentHandlerLimitSetting _concurrentHandlerLimit;
        private readonly PrefetchCountSetting _prefetchCountSetting;
        private readonly ILogger _logger;
        private SubscriptionClient _subscriptionClient;
        private IMessageDispatcher _messageDispatcher;

        public OnReceiveSubscriptionMessageReceiver(IQueueManager queueManager,
                                                 string topicPath,
                                                 string subscriptionName,
                                                 ConcurrentHandlerLimitSetting concurrentHandlerLimit,
                                                 PrefetchCountSetting prefetchCountSetting,
                                                 ILogger logger)         
        {
            _queueManager = queueManager;
            _topicPath = topicPath;
            _subscriptionName = subscriptionName;
            _concurrentHandlerLimit = concurrentHandlerLimit;
            _prefetchCountSetting = prefetchCountSetting;
            _logger = logger;
        }

        public override string ToString()
        {
            return "{0}/{1}".FormatWith(_topicPath, _subscriptionName);
        }

        public async Task SubscribeToQueue()
        {
            if (_messageDispatcher == null) throw new Exception("No dispatcher registered");

            var options = new OnMessageOptions { MaxConcurrentCalls = _concurrentHandlerLimit };
            var receiver = await GetSubscriptionClient();
            receiver.OnMessageAsync(async (message) =>
            {
                try
                {
                    await _messageDispatcher.DispatchAsync(message);
                }
                catch (Exception e)
                {
                    message.Abandon();
                    _logger.Error("Error handling message {0}. Exception: {1}", message.MessageId, e.Message);
                }

            }, options);
        }

        public void RegisterDispatcher(IMessageDispatcher dispatcher)
        {
            _messageDispatcher = dispatcher;
        }

        private async Task<SubscriptionClient> GetSubscriptionClient()
        {
            if (_subscriptionClient != null) return _subscriptionClient;

            _subscriptionClient = await _queueManager.CreateSubscriptionReceiver(_topicPath, _subscriptionName);
            _subscriptionClient.PrefetchCount = _prefetchCountSetting;
            return _subscriptionClient;
        }

        private void DiscardSubscriptionClient()
        {
            var subscriptionClient = _subscriptionClient;
            _subscriptionClient = null;

            if (subscriptionClient == null) return;
            if (subscriptionClient.IsClosed) return;

            subscriptionClient.Close();
        }

        public void Dispose()
        {
            try
            {             
                DiscardSubscriptionClient();
            }
            catch (MessagingEntityNotFoundException)
            {
            }
            catch (ObjectDisposedException)
            {
            }
        }
    }
}