using System;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;

namespace Zombus.Infrastructure.MessageSendersAndReceivers
{
    internal class ZombusTopicMessageSender : BatchingMessageSender
    {
        private readonly IQueueManager _queueManager;
        private readonly string _topicPath;
        private readonly ILogger _logger;

        private TopicClient _topicClient;

        public ZombusTopicMessageSender(IQueueManager queueManager, string topicPath, ILogger logger)
            : base()
        {
            _queueManager = queueManager;
            _topicPath = topicPath;
            _logger = logger;
        }

        protected override async Task SendBatch(BrokeredMessage[] toSend)
        {
            var topicClient = await GetTopicClient();

            _logger.Debug("Flushing outbound message queue {0} ({1} messages)", _topicPath, toSend.Length);
            try
            {
                await topicClient.SendBatchAsync(toSend);
            }
            catch (Exception)
            {
                DiscardTopicClient();
                throw;
            }
        }

        private async Task<TopicClient> GetTopicClient()
        {
            if (_topicClient != null) return _topicClient;

            _topicClient = await _queueManager.CreateTopicSender(_topicPath);
            return _topicClient;
        }

        private void DiscardTopicClient()
        {
            var topicClient = _topicClient;
            _topicClient = null;

            if (topicClient == null) return;
            if (topicClient.IsClosed) return;

            try
            {
                _logger.Info("Discarding message sender for {TopicPath}", _topicPath);
                topicClient.Close();
            }
            catch (Exception exc)
            {
                _logger.Error(exc, "Failed to close TopicClient instance before discarding it.");
            }
        }

        protected override void Dispose(bool disposing)
        {
            try
            {
                DiscardTopicClient();
            }
            finally
            {
                base.Dispose(disposing);
            }
        }
    }
}