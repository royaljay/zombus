using System.Linq;
using Microsoft.ServiceBus.Messaging;
using Zombus.Extensions;
using Zombus.Configuration.Settings;
using Zombus.DependencyResolution;
using Zombus.Infrastructure.PropertyInjection;

namespace Zombus.Interceptors.Outbound
{
    internal class OutboundInterceptorFactory : IOutboundInterceptorFactory
    {
        private readonly GlobalOutboundInterceptorTypesSetting _globalOutboundInterceptorTypes;
        private readonly IPropertyInjector _propertyInjector;

        public OutboundInterceptorFactory(GlobalOutboundInterceptorTypesSetting globalOutboundInterceptorTypes, IPropertyInjector propertyInjector)
        {
            _globalOutboundInterceptorTypes = globalOutboundInterceptorTypes;
            _propertyInjector = propertyInjector;
        }

        public IOutboundInterceptor[] CreateInterceptors(IDependencyResolverScope scope, BrokeredMessage brokeredMessage)
        {
            return _globalOutboundInterceptorTypes
                .Value
                .Select(t => (IOutboundInterceptor) scope.Resolve(t))
                .Do(interceptor => _propertyInjector.Inject(interceptor, brokeredMessage))
                .ToArray();
        }
    }
}