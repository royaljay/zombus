﻿using Microsoft.ServiceBus.Messaging;
using Zombus.DependencyResolution;

namespace Zombus.Interceptors.Outbound
{
    internal interface IOutboundInterceptorFactory
    {
        IOutboundInterceptor[] CreateInterceptors(IDependencyResolverScope scope, BrokeredMessage brokeredMessage);
    }
}