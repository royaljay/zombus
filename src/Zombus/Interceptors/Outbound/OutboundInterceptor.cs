using System;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;
using Zombus.MessageContracts;

namespace Zombus.Interceptors.Outbound
{
    public abstract class OutboundInterceptor : IOutboundInterceptor
    {
        public virtual int Priority
        {
            get { return int.MaxValue; }
        }

        public virtual async Task OnCommandSending<TBusCommand>(TBusCommand busCommand, BrokeredMessage brokeredMessage) where TBusCommand : IBusCommand
        {
        }

        public virtual async Task OnCommandSent<TBusCommand>(TBusCommand busCommand, BrokeredMessage brokeredMessage) where TBusCommand : IBusCommand
        {
        }

        public virtual async Task OnCommandSendingError<TBusCommand>(TBusCommand busCommand, BrokeredMessage brokeredMessage, Exception exception) where TBusCommand : IBusCommand
        {
        }

        public virtual async Task OnEventPublishing<TBusEvent>(TBusEvent busEvent, BrokeredMessage brokeredMessage) where TBusEvent : IBusEvent
        {
        }

        public virtual async Task OnEventPublished<TBusEvent>(TBusEvent busEvent, BrokeredMessage brokeredMessage) where TBusEvent : IBusEvent
        {
        }

        public virtual async Task OnEventPublishingError<TBusEvent>(TBusEvent busEvent, BrokeredMessage brokeredMessage, Exception exception) where TBusEvent : IBusEvent
        {
        }
    }
}