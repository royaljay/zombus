﻿using System;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;
using Zombus.Extensions;
using Zombus.MessageContracts;
using Zombus.Interceptors.Outbound;
using Zombus.MessageContracts.ControlMessages;
using Zombus.PropertyInjection;

namespace Zombus.Interceptors
{
    public class OutboundAuditingInterceptor : OutboundInterceptor, IRequireBus, IRequireDateTime
    {
        public IBus Bus { get; set; }
        public Func<DateTimeOffset> UtcNow { get; set; }

        public override async Task OnCommandSent<TBusCommand>(TBusCommand busCommand, BrokeredMessage brokeredMessage)
        {
            var auditEvent = CreateAuditEvent(busCommand, brokeredMessage);
            await Bus.PublishAsync(auditEvent);
        }

        public override async Task OnEventPublishing<TBusEvent>(TBusEvent busEvent, BrokeredMessage brokeredMessage)
        {
            // Quis custodiet ipsos custodes? ;)
            var auditEvent = busEvent as AuditEvent;
            if (auditEvent == null) return;

            brokeredMessage.Properties["AuditedMessageType"] = auditEvent.MessageType;
        }
        public override async Task OnEventPublished<TBusEvent>(TBusEvent busEvent, BrokeredMessage brokeredMessage)
        {
            // Quis custodiet ipsos custodes? ;)
            if (busEvent is AuditEvent) return;

            var auditEvent = CreateAuditEvent(busEvent, brokeredMessage);
            await Bus.PublishAsync(auditEvent);
        }

        private AuditEvent CreateAuditEvent(object message, BrokeredMessage brokeredMessage)
        {
            var timestamp = UtcNow();
            var auditEvent = new AuditEvent(message.GetType().FullName, message, brokeredMessage.ExtractProperties(), timestamp);
            return auditEvent;
        }
    }
}