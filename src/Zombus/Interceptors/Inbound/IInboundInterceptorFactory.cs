﻿using Microsoft.ServiceBus.Messaging;
using Zombus.DependencyResolution;

namespace Zombus.Interceptors.Inbound
{
    internal interface IInboundInterceptorFactory
    {
        IInboundInterceptor[] CreateInterceptors(IDependencyResolverScope scope, object handler, object message, BrokeredMessage brokeredMessage);
    }
}