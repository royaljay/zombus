using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Zombus.Configuration;
using Zombus.Infrastructure.Commands;
using Zombus.Infrastructure.Events;
using Zombus.Infrastructure.Heartbeat;
using Zombus.MessageContracts;
using Zombus.MessageContracts.Exceptions;

namespace Zombus
{
    public class Bus : IBus, IDisposable
    {
        private readonly ILogger _logger;
        private readonly ICommandSender _commandSender;
        private readonly IEventSender _eventSender;
        private readonly IMessageReceiverManager _messageReceiverManager;
        private readonly IDeadLetterQueues _deadLetterQueues;
        private readonly IHeartbeat _heartbeat;

        private readonly object _mutex = new object();
        private bool _isRunning;

        internal Bus(ILogger logger,
                     ICommandSender commandSender,
                     IEventSender eventSender,
                     IMessageReceiverManager messageReceiverManager,
                     IDeadLetterQueues deadLetterQueues,
                     IHeartbeat heartbeat)
        {
            _logger = logger;
            _commandSender = commandSender;
            _eventSender = eventSender;
            _deadLetterQueues = deadLetterQueues;
            _heartbeat = heartbeat;
            _messageReceiverManager = messageReceiverManager;

            Started += async delegate { await _heartbeat.Start(); };
            Stopping += async delegate { await _heartbeat.Stop(); };
        }

        public async Task SendAsync<TBusCommand>(TBusCommand busCommand) where TBusCommand : IBusCommand
        {
            await _commandSender.SendAsync(busCommand);
        }

        public async Task SendAtAsync<TBusCommand>(TBusCommand busCommand, DateTimeOffset deliveryTime) where TBusCommand : IBusCommand
        {
            await _commandSender.SendAsyncAt(busCommand, deliveryTime);
        }

        public async Task PublishAsync<TBusEvent>(TBusEvent busEvent) where TBusEvent : IBusEvent
        {
            await _eventSender.PublishAsync(busEvent);
        }

        public IDeadLetterQueues DeadLetterQueues
        {
            get { return _deadLetterQueues; }
        }

        public EventHandler<EventArgs> Starting;
        public EventHandler<EventArgs> Started;
        public EventHandler<EventArgs> Stopping;
        public EventHandler<EventArgs> Stopped;

        public async Task Start(MessageReceiverTypes messageReceiverTypes = MessageReceiverTypes.Default)
        {
            lock (_mutex)
            {
                if (_isRunning) return;
                _isRunning = true;
            }

            _logger.Debug("Bus starting...");

            try
            {
                var startingHandler = Starting;
                if (startingHandler != null) startingHandler(this, EventArgs.Empty);

                await _messageReceiverManager.SubscribeAllReceivers(messageReceiverTypes);
            }
            catch (AggregateException aex)
            {
                _logger.Error(aex, "Failed to start bus.");
                throw new BusException("Failed to start bus", aex);
            }

            var startedHandler = Started;
            if (startedHandler != null) startedHandler(this, EventArgs.Empty);

            _logger.Info("Bus started.");
        }

        public async Task Stop(MessageReceiverTypes messageReceiverTypes = MessageReceiverTypes.All)
        {
            lock (_mutex)
            {
                if (!_isRunning) return;
                _isRunning = false;
            }

            _logger.Debug("Bus stopping...");

            try
            {
                var stoppingHandler = Stopping;
                if (stoppingHandler != null) stoppingHandler(this, EventArgs.Empty);

                await _messageReceiverManager.Stop(messageReceiverTypes);
            }
            catch (AggregateException aex)
            {
                throw new BusException("Failed to stop bus", aex);
            }

            var stoppedHandler = Stopped;
            if (stoppedHandler != null) stoppedHandler(this, EventArgs.Empty);

            _logger.Info("Bus stopped.");
        }

        public EventHandler<EventArgs> Disposing;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing) return;

            var handler = Disposing;
            if (handler != null) handler(this, EventArgs.Empty);
        }
    }
}