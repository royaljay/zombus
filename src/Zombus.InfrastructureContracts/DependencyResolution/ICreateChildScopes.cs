﻿namespace Zombus.DependencyResolution
{
    public interface ICreateChildScopes
    {
        IDependencyResolverScope CreateChildScope();
    }
}