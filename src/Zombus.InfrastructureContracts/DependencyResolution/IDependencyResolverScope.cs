﻿using System;

namespace Zombus.DependencyResolution
{
    public interface IDependencyResolverScope : ICreateChildScopes, IDisposable
    {
        TComponent Resolve<TComponent>();
        object Resolve(Type componentType);
    }
}