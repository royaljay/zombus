﻿using System;

namespace Zombus.DependencyResolution
{
    public interface IDependencyResolver : ICreateChildScopes, IDisposable
    {
    }
}