using System;

namespace Zombus.PropertyInjection
{
    public interface IRequireDateTime
    {
        Func<DateTimeOffset> UtcNow { get; set; }
    }
}