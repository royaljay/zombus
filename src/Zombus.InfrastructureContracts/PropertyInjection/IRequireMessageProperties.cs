using System.Collections.Generic;

namespace Zombus.PropertyInjection
{
    public interface IRequireMessageProperties
    {
        IDictionary<string, object> MessageProperties { get; set; }
    }
}