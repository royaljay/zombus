namespace Zombus.PropertyInjection
{
    public interface IRequireLargeMessageBodyStore
    {
        ILargeMessageBodyStore LargeMessageBodyStore { get; set; }
    }
}