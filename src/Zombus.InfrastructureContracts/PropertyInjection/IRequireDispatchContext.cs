﻿namespace Zombus.PropertyInjection
{
    public interface IRequireDispatchContext
    {
        IDispatchContext DispatchContext { get; set; }
    }
}