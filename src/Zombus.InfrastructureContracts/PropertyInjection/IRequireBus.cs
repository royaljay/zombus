namespace Zombus.PropertyInjection
{
    public interface IRequireBus
    {
        IBus Bus { get; set; }
    }
}