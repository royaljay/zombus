﻿

using Microsoft.ServiceBus.Messaging;

namespace Zombus.PropertyInjection
{
    public interface IRequireBrokeredMessage
    {
        BrokeredMessage BrokeredMessage { get; set; }
    }
}