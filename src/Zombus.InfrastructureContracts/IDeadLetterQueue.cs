﻿using System.Threading.Tasks;

// ReSharper disable CheckNamespace

namespace Zombus
// ReSharper restore CheckNamespace
{
    public interface IDeadLetterQueue
    {
        Task<TBusMessageContract> Pop<TBusMessageContract>() where TBusMessageContract : class;
    }
}