namespace Zombus.Routing
{
    public enum QueueOrTopic
    {
        Queue,
        Topic,
    }
}