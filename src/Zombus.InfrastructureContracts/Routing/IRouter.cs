﻿using System;

namespace Zombus.Routing
{
    public interface IRouter
    {
        string Route(Type messageType, QueueOrTopic queueOrTopic);
    }
}