﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Zombus.MessageContracts;

// ReSharper disable CheckNamespace

namespace Zombus
// ReSharper restore CheckNamespace
{
    public interface IBus
    {
        Task SendAsync<TBusCommand>(TBusCommand busCommand) where TBusCommand : IBusCommand;

        Task SendAtAsync<TBusCommand>(TBusCommand busCommand, DateTimeOffset deliveryTime) where TBusCommand : IBusCommand;

        Task PublishAsync<TBusEvent>(TBusEvent busEvent) where TBusEvent : IBusEvent;

        IDeadLetterQueues DeadLetterQueues { get; }
    }
}