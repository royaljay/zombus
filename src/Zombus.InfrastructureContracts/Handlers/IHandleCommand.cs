﻿using System.Threading.Tasks;
using Zombus.MessageContracts;

namespace Zombus.Handlers
{
    public interface IHandleCommand<TBusCommand> where TBusCommand : IBusCommand
    {
        Task Handle(TBusCommand busCommand);
    }
}