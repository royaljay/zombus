﻿using System.Threading.Tasks;
using Zombus.MessageContracts;

namespace Zombus.Handlers
{
    public interface IHandleCompetingEvent<TBusEvent> where TBusEvent : IBusEvent
    {
        Task Handle(TBusEvent busEvent);
    }
}