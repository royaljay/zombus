﻿using System.Threading.Tasks;
using Zombus.MessageContracts;

namespace Zombus.Handlers
{
    public interface IHandleMulticastEvent<TBusEvent> where TBusEvent : IBusEvent
    {
        Task Handle(TBusEvent busEvent);
    }
}