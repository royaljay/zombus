﻿namespace Zombus.Handlers
{
    public interface ILongRunningTask
    {
        bool IsAlive { get; }
    }
}