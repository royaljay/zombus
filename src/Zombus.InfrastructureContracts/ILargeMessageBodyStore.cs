using System;
using System.Threading.Tasks;

namespace Zombus
{
    public interface ILargeMessageBodyStore
    {
        Task<string> Store(string id, byte[] bytes, DateTimeOffset expiresAfter);
        Task<byte[]> Retrieve(string storageKey);
        Task Delete(string storageKey);
    }
}