﻿using Zombus.MessageContracts;

namespace Zombus.UnitTests.DependencyResolverTests.DisposableComponents.MessageContracts
{
    public class NullCommand : IBusCommand
    {
    }
}