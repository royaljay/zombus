﻿using System;
using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.UnitTests.DependencyResolverTests.DisposableComponents.MessageContracts;

namespace Zombus.UnitTests.DependencyResolverTests.DisposableComponents.Handlers
{
    public class DisposableHandler : IHandleCommand<NullCommand>, IDisposable
    {
        public DisposableHandler()
        {
            IsDisposed = false;
        }

        public void Dispose()
        {
            IsDisposed = true;
        }

        public bool IsDisposed { get; private set; }

        public async Task Handle(NullCommand busCommand)
        {
        }
    }
}