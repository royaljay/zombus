﻿using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.Tests.Common;
using Zombus.UnitTests.DependencyResolverTests.AllComponentTypes.MessageContracts;

#pragma warning disable 4014

namespace Zombus.UnitTests.DependencyResolverTests.AllComponentTypes.Handlers
{
    public class BrokerTestCommandHandler : IHandleCommand<FooCommand>
    {
        public async Task Handle(FooCommand busCommand)
        {
            MethodCallCounter.RecordCall<BrokerTestCommandHandler>(h => h.Handle(busCommand));
        }
    }
}