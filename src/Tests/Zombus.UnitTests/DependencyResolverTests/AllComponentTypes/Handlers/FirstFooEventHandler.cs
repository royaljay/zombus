﻿using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.Tests.Common;
using Zombus.UnitTests.DependencyResolverTests.AllComponentTypes.MessageContracts;

#pragma warning disable 4014

namespace Zombus.UnitTests.DependencyResolverTests.AllComponentTypes.Handlers
{
    public class FirstFooEventHandler : IHandleCompetingEvent<FooEvent>
    {
        public async Task Handle(FooEvent busEvent)
        {
            MethodCallCounter.RecordCall<FirstFooEventHandler>(h => h.Handle(busEvent));
        }
    }
}