using Autofac;
using Zombus.Configuration;
using Zombus.DependencyResolution;

namespace Zombus.UnitTests.DependencyResolverTests.TestInfrastructure.DependencyResolverFactories
{
    public class AutofacDependencyResolverFactory : IDependencyResolverFactory
    {
        public IDependencyResolver Create(ITypeProvider typeProvider)
        {
            var builder = new ContainerBuilder();
            builder.RegisterZombus(typeProvider);
            return builder.Build().Resolve<IDependencyResolver>();
        }
    }
}