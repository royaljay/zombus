using Zombus.DependencyResolution;
using Zombus.Infrastructure.DependencyResolution;

namespace Zombus.UnitTests.DependencyResolverTests.TestInfrastructure.DependencyResolverFactories
{
    public class DefaultDependencyResolverFactory : IDependencyResolverFactory
    {
        public IDependencyResolver Create(ITypeProvider typeProvider)
        {
            return new DependencyResolver(typeProvider);
        }

        public void Dispose()
        {
        }
    }
}