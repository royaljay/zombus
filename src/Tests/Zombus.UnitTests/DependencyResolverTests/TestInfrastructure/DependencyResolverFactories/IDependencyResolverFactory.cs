using Zombus.DependencyResolution;

namespace Zombus.UnitTests.DependencyResolverTests.TestInfrastructure.DependencyResolverFactories
{
    public interface IDependencyResolverFactory
    {
        IDependencyResolver Create(ITypeProvider typeProvider);
    }
}