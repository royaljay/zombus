﻿using System.Linq;
using System.Threading.Tasks;
using Zombus.Configuration.Settings;
using NSubstitute;
using NUnit.Framework;
using Shouldly;
using Zombus.Configuration;
using Zombus.Infrastructure.Commands;
using Zombus.Infrastructure.Events;
using Zombus.Infrastructure.Heartbeat;
using Zombus.MessageContracts;
using Zombus.UnitTests.BatchSendingTests.MessageContracts;

namespace Zombus.UnitTests.BatchSendingTests
{
    [TestFixture]
    public class WhenPublishingACollectionOfEvents : SpecificationForAsync<Bus>
    {
        private IEventSender _eventSender;

        protected override Task<Bus> Given()
        {
            var logger = Substitute.For<ILogger>();
            var commandSender = Substitute.For<ICommandSender>();
            
            _eventSender = Substitute.For<IEventSender>();
            var messagePumpsManager = Substitute.For<IMessageReceiverManager>();
            var deadLetterQueues = Substitute.For<IDeadLetterQueues>();

            var bus = new Bus(logger,
                              commandSender,
                              _eventSender,
                              messagePumpsManager,
                              deadLetterQueues,
                              Substitute.For<IHeartbeat>());
            return Task.FromResult(bus);
        }

        protected override async Task When()
        {
            var events = new IBusEvent[] {new FooEvent(), new BarEvent(), new BazEvent()};
            await Subject.PublishAll(events);
        }

        [Test]
        public void TheEventSenderShouldHaveReceivedThreeCalls()
        {
            _eventSender.ReceivedCalls().Count().ShouldBe(3);
        }
    }
}