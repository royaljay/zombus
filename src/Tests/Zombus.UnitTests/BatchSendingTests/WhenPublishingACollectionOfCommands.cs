using System.Linq;
using System.Threading.Tasks;
using Zombus.Configuration.Settings;
using NSubstitute;
using NUnit.Framework;
using Shouldly;
using Zombus.Configuration;
using Zombus.Infrastructure.Commands;
using Zombus.Infrastructure.Events;
using Zombus.Infrastructure.Heartbeat;
using Zombus.MessageContracts;
using Zombus.UnitTests.BatchSendingTests.MessageContracts;

namespace Zombus.UnitTests.BatchSendingTests
{
    [TestFixture]
    public class WhenPublishingACollectionOfCommands : SpecificationForAsync<Bus>
    {
        private ICommandSender _commandSender;

        protected override Task<Bus> Given()
        {
            var logger = Substitute.For<ILogger>();
            _commandSender = Substitute.For<ICommandSender>();
           
            var eventSender = Substitute.For<IEventSender>();
            var messagePumpsManager = Substitute.For<IMessageReceiverManager>();
            var deadLetterQueues = Substitute.For<IDeadLetterQueues>();

            var bus = new Bus(logger,
                              _commandSender,
                            
                              eventSender,
                              messagePumpsManager,
                              deadLetterQueues,
                              Substitute.For<IHeartbeat>());
            return Task.FromResult(bus);
        }

        protected override async Task When()
        {
            var commands = new IBusCommand[] {new FooCommand(), new BarCommand(), new BazCommand()};
            await Subject.SendAll(commands);
        }

        [Test]
        public void TheCommandSenderShouldHaveReceivedThreeCalls()
        {
            _commandSender.ReceivedCalls().Count().ShouldBe(3);
        }
    }
}