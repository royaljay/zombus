using System.Linq;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using Shouldly;
using Zombus.Configuration.LargeMessages.Settings;
using Zombus.Configuration.Settings;
using Zombus.Infrastructure;
using Zombus.Infrastructure.BrokeredMessageServices;
using Zombus.Infrastructure.BrokeredMessageServices.Compression;
using Zombus.Infrastructure.BrokeredMessageServices.LargeMessages;
using Zombus.Infrastructure.BrokeredMessageServices.Serialization;
using Zombus.Infrastructure.Dispatching;
using Zombus.Infrastructure.Events;
using Zombus.Infrastructure.MessageSendersAndReceivers;
using Zombus.Infrastructure.Routing;
using Zombus.MessageContracts;
using Zombus.Tests.Common;
using Zombus.UnitTests.BatchSendingTests.MessageContracts;

namespace Zombus.UnitTests.BatchSendingTests
{
    [TestFixture]
    internal class WhenPublishingACollectionOfEventsViaTheBusEventSender : SpecificationForAsync<BusEventSender>
    {
        private IMessageSender _messageSender;

        protected override Task<BusEventSender> Given()
        {
            _messageSender = Substitute.For<IMessageSender>();

            var messagingFactory = Substitute.For<IMessagingFactory>();
            messagingFactory.GetTopicSender(Arg.Any<string>()).Returns(ci => _messageSender);

            var clock = new SystemClock();
            var typeProvider = new TestHarnessTypeProvider(new[] {GetType().Assembly}, new[] {GetType().Namespace});
            var serializer = new DataContractSerializer(typeProvider);
           
            var brokeredMessageFactory = new BrokeredMessageFactory(new DefaultMessageTimeToLiveSetting(),
                                                                    new MaxLargeMessageSizeSetting(),
                                                                    new MaxSmallMessageSizeSetting(),
                                                                    clock,
                                                                    new NullCompressor(),
                                                                    new DispatchContextManager(),
                                                                    new UnsupportedLargeMessageBodyStore(),
                                                                    serializer,
                                                                    typeProvider);
            var logger = Substitute.For<ILogger>();
            var knownMessageTypeVerifier = Substitute.For<IKnownMessageTypeVerifier>();
            var router = new DestinationPerMessageTypeRouter();
            var dependencyResolver = new NullDependencyResolver();
            var outboundInterceptorFactory = new NullOutboundInterceptorFactory();
            var busCommandSender = new BusEventSender(brokeredMessageFactory,
                                                      dependencyResolver,
                                                      knownMessageTypeVerifier,
                                                      logger,
                                                      messagingFactory,
                                                      outboundInterceptorFactory,
                                                      router);
            return Task.FromResult(busCommandSender);
        }

        protected override async Task When()
        {
            var events = new IBusEvent[] {new FooEvent(), new BarEvent(), new BazEvent()};

            foreach (var e in events)
            {
                await Subject.PublishAsync(e);
            }
        }

        [Test]
        public void TheEventSenderShouldHaveReceivedThreeCalls()
        {
            _messageSender.ReceivedCalls().Count().ShouldBe(3);
        }
    }
}