﻿using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.UnitTests.InfrastructureTests.MessageContracts;

namespace Zombus.UnitTests.InfrastructureTests.Handlers
{
    public class MyLongNamedHandler : IHandleMulticastEvent<MyEventWithALongName>
    {
        public async Task Handle(MyEventWithALongName busEvent)
        {
            
        }
    }
}