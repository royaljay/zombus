﻿using System;
using NSubstitute;
using NUnit.Framework;
using Zombus.Infrastructure;
using Zombus.UnitTests.DelayedSendingTests.MessageContracts;

namespace Zombus.UnitTests.DelayedSendingTests
{
    [TestFixture]
    public class WhenSendingACommandThatIsDelayedByAnHour : SpecificationFor<IBus>
    {
        private FooCommand _fooCommand;
        private DateTimeOffset _expectedDeliveryTime;

        protected override IBus Given()
        {
            return Substitute.For<IBus>();
        }

        protected override void When()
        {
            var lyingClock = Substitute.For<IClock>();
            var now = new DateTimeOffset(2014, 02, 09, 18, 04, 58, TimeSpan.FromHours(10));
            lyingClock.UtcNow.ReturnsForAnyArgs(now);

            DelayedSendingExtensions.SetClockStrategy(lyingClock);

            _fooCommand = new FooCommand();
            var delay = TimeSpan.FromDays(2);
            _expectedDeliveryTime = now.Add(delay);

            Subject.SendAfter(_fooCommand, TimeSpan.FromDays(2));
        }

        [Test]
        public void TheCalculatedTimeShouldBeCorrect()
        {
            Subject.Received().SendAtAsync(_fooCommand, _expectedDeliveryTime);
        }
    }
}