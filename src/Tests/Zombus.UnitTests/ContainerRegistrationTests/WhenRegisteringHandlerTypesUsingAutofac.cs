﻿using System.Threading.Tasks;
using Autofac;
using Zombus.Configuration;
using NUnit.Framework;
using Zombus.Infrastructure;

namespace Zombus.UnitTests.ContainerRegistrationTests
{
    [TestFixture]
    public class WhenRegisteringHandlerTypesUsingAutofac
    {
        [Test]
        public async Task NothingShouldGoBang()
        {
            var typeProvider = new AssemblyScanningTypeProvider(GetType().Assembly);

            var builder = new ContainerBuilder();
            builder.RegisterZombus(typeProvider);

            using (builder.Build()) { }
        }
    }
}