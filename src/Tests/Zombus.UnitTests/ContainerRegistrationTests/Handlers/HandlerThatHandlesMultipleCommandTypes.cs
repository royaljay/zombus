﻿using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.UnitTests.ContainerRegistrationTests.MessageContracts;

namespace Zombus.UnitTests.ContainerRegistrationTests.Handlers
{
    public class HandlerThatHandlesMultipleCommandTypes : IHandleCommand<FooCommand>, IHandleCommand<BarCommand>
    {
        public async Task Handle(FooCommand busCommand)
        {
        }

        public async Task Handle(BarCommand busCommand)
        {
        }
    }
}