﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Shouldly;
using Zombus.Handlers;

namespace Zombus.UnitTests.Conventions
{
    [TestFixture]
    public class CoreHandlerInterfaces
    {
        [Test]
        [TestCaseSource(typeof (TestCases))]
        public void MustBeInTheCorrectNamespaceNamespace(Type type)
        {
            type.Namespace.ShouldBe("Zombus.Handlers");
        }

        internal class TestCases : IEnumerable<TestCaseData>
        {
            public IEnumerator<TestCaseData> GetEnumerator()
            {
                var coreInfrastructureInterfaces = new[]
                                                   {
                                                       typeof (IHandleCommand<>),
                                                       typeof (IHandleMulticastEvent<>),
                                                       typeof (IHandleCompetingEvent<>)
                                                   };

                return coreInfrastructureInterfaces
                    .Select(t => new TestCaseData(t).SetName(t.FullName))
                    .GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }
    }
}