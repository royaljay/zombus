﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Shouldly;
using Zombus.Infrastructure;

namespace Zombus.UnitTests.Conventions
{
    [TestFixture]
    public class AllInterfacesInTheInfrastructureNamespace
    {
        [Test]
        [TestCaseSource(typeof (TestCases))]
        public void MustBeInternal(Type interfaceType)
        {
            interfaceType.IsPublic.ShouldBe(false);
        }

        private class TestCases : IEnumerable<TestCaseData>
        {
            public IEnumerator<TestCaseData> GetEnumerator()
            {
                var referenceType = typeof (IEventBasedReceiver);

                return referenceType.Assembly.GetTypes()
                                    .Where(t => t.Namespace != null)
                                    .Where(t => t.Namespace == referenceType.Namespace || t.Namespace.StartsWith(referenceType.Namespace + "."))
                                    .Where(t => t.IsInterface)
                                    .Select(t => new TestCaseData(t)
                                                     .SetName(t.FullName)
                    ).GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }
    }
}