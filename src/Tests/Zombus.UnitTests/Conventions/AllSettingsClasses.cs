﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Zombus.Extensions;
using NUnit.Framework;
using Shouldly;
using Zombus.Configuration.Settings;

namespace Zombus.UnitTests.Conventions
{
    [TestFixture]
    public class AllSettingsClasses
    {
        private readonly string _referenceNamespace = typeof (Setting<>).Namespace;

        [Test]
        [TestCaseSource(typeof (TestCases))]
        public void ShouldBeInASettingsNamespaceUnderConfiguration(Type settingType)
        {
            settingType.Namespace.ShouldContain(".Configuration.");
            settingType.Namespace.ShouldEndWith(".Settings");
        }

        [Test]
        [TestCaseSource(typeof (TestCases))]
        public void ShouldBePublic(Type settingType)
        {
            settingType.IsPublic.ShouldBe(true);
        }

        internal class TestCases : IEnumerable<TestCaseData>
        {
            public IEnumerator<TestCaseData> GetEnumerator()
            {
                return typeof (Setting<>).Assembly
                                         .GetTypes()
                                         .Where(t => t.IsClosedTypeOf(typeof (Setting<>)))
                                         .Select(t => new TestCaseData(t)
                                                     .SetName(t.FullName))
                                         .GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }
    }
}