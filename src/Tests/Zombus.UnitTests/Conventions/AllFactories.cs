﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Zombus.Extensions;
using NUnit.Framework;
using Shouldly;
using Zombus.Configuration;
using Zombus.DependencyResolution;
using Zombus.Infrastructure;
using Zombus.Infrastructure.BrokeredMessageServices;
using Zombus.Interceptors.Inbound;
using Zombus.Interceptors.Outbound;

namespace Zombus.UnitTests.Conventions
{
    [TestFixture]
    public class AllFactories
    {
        [Test]
        [TestCaseSource(typeof (TestCases))]
        public void MustImplementICreateComponents(Type factoryType)
        {
            typeof (ICreateComponents).IsAssignableFrom(factoryType).ShouldBe(true);
        }

        internal class TestCases : IEnumerable<TestCaseData>
        {
            public IEnumerator<TestCaseData> GetEnumerator()
            {
                return typeof (Bus).Assembly
                                   .GetTypes()
                                   .Where(t => t.Name.EndsWith("Factory"))
                                   .Where(t => GetExcludedTypes().Contains(t) == false)
                                   .Where(t => t.IsInstantiable())
                                   .Where(t => t.GetCustomAttribute<ObsoleteAttribute>() == null)
                                   .Where(t => !t.IsAssignableFrom(typeof (IDependencyResolver)))
                                   .Select(t => new TestCaseData(t)
                                                    .SetName(t.FullName))
                                   .GetEnumerator();
            }

            private IEnumerable<Type> GetExcludedTypes()
            {
                yield return typeof (BrokeredMessageFactory);
                yield return typeof (InboundInterceptorFactory);
                yield return typeof (OutboundInterceptorFactory);
                yield return typeof (MessageDispatcherFactory);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }
    }
}