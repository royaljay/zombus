﻿using System.Linq;
using Autofac;
using Microsoft.ServiceBus.Messaging;
using Zombus.Configuration;
using Zombus.Infrastructure.Dispatching;
using NSubstitute;
using NUnit.Framework;
using Shouldly;
using Zombus.Configuration.Settings;
using Zombus.DependencyResolution;
using Zombus.Infrastructure;
using Zombus.Infrastructure.PropertyInjection;
using Zombus.Interceptors.Outbound;

namespace Zombus.UnitTests.InterceptorTests
{
    [TestFixture]
    public class WhenRegisteringInterceptorsWithAutofac
    {
        private class DummyInterceptor : OutboundInterceptor
        {
        }

        [Test]
        public void TheyShouldBeResolvable()
        {
            var interceptorTypes = new[] {typeof (DummyInterceptor)};

            var builder = new ContainerBuilder();
            var typeProvider = Substitute.For<ITypeProvider>();
            typeProvider.InterceptorTypes.Returns(interceptorTypes);

            builder.RegisterZombus(typeProvider);

            using (var container = builder.Build())
            using (var dependencyResolver = container.Resolve<IDependencyResolver>())
            using (var scope = dependencyResolver.CreateChildScope())
            {
                var interceptorSetting = new GlobalOutboundInterceptorTypesSetting
                                         {
                                             Value = interceptorTypes
                                         };
                var outboundInterceptorFactory = new OutboundInterceptorFactory(interceptorSetting,
                                                                                new PropertyInjector(Substitute.For<IClock>(),
                                                                                    Substitute.For<IDispatchContextManager>(),
                                                                                                     Substitute.For<ILargeMessageBodyStore>()));

                var dummyBrokeredMessage = new BrokeredMessage();
                var interceptors = outboundInterceptorFactory.CreateInterceptors(scope, dummyBrokeredMessage);

                interceptors.Count().ShouldBe(1);
            }
        }
    }
}