﻿using System.Threading.Tasks;
using NSubstitute;
using Zombus.Configuration.LargeMessages.Settings;
using Zombus.Configuration.Settings;
using Zombus.Infrastructure;
using Zombus.Infrastructure.BrokeredMessageServices;
using Zombus.Infrastructure.BrokeredMessageServices.Compression;
using Zombus.Infrastructure.BrokeredMessageServices.LargeMessages;
using Zombus.Infrastructure.Dispatching;
using Zombus.Tests.Common;

namespace Zombus.UnitTests.BrokeredMessageFactoryTests
{
    internal abstract class GivenABrokeredMessageFactory : SpecificationForAsync<BrokeredMessageFactory>
    {
        private IClock _clock;
        private ISerializer _serializer;
       

        protected override async Task<BrokeredMessageFactory> Given()
        {
            _clock = Substitute.For<IClock>();
            _serializer = Substitute.For<ISerializer>();


            return new BrokeredMessageFactory(new DefaultMessageTimeToLiveSetting(),
                                              new MaxLargeMessageSizeSetting(),
                                              new MaxSmallMessageSizeSetting(),
                                              _clock,
                                              new NullCompressor(),
                                              new DispatchContextManager(),
                                              new UnsupportedLargeMessageBodyStore(),
                                              _serializer,
                                              new TestHarnessTypeProvider(new[] {GetType().Assembly}, new[] {GetType().Namespace})
                );
        }
    }
}