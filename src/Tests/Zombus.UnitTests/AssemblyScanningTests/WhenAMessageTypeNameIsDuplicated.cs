﻿using System.Reflection;
using NUnit.Framework;
using Shouldly;
using Zombus.Infrastructure;
using Zombus.UnitTests.TestAssemblies.MessageContracts;

namespace Zombus.UnitTests.AssemblyScanningTests
{
    [TestFixture]
    public class WhenAMessageTypeNameIsDuplicated
    {
        [Test]
        public void ValidationShouldFail()
        {
            var assemblyScanningTypeProvider = new AssemblyScanningTypeProvider(Assembly.GetAssembly(typeof (DuplicateMessageType)));
            assemblyScanningTypeProvider.Validate().ShouldNotBeEmpty();
        }
    }
}