﻿using System.Linq;
using NUnit.Framework;
using Shouldly;
using Zombus.Infrastructure;
using Zombus.UnitTests.TestAssemblies.MessageContracts.Serialization;

namespace Zombus.UnitTests.AssemblyScanningTests
{
    [TestFixture]
    public class WhenAUnserializableAMessageThatIsInAnAssemblyThatIsNotIncluded
    {
        [Test]
        public void ValidationShouldFail()
        {
            var assemblyScanningTypeProvider = new AssemblyScanningTypeProvider(typeof (UnserializableCommandWhoseAssemblyShouldNotBeIncluded).Assembly);
            assemblyScanningTypeProvider.Validate().ShouldNotBeEmpty();
        }

        [Test]
        public void TheMessageShouldMentionTheOffendingTypeByName()
        {
            var assemblyScanningTypeProvider = new AssemblyScanningTypeProvider(typeof (UnserializableCommandWhoseAssemblyShouldNotBeIncluded).Assembly);

            var validationErrors = assemblyScanningTypeProvider.Validate().ToArray();

            validationErrors.ShouldContain(e => e.Contains(typeof (UnserializableCommandWhoseAssemblyShouldNotBeIncluded).FullName));
        }
    }
}