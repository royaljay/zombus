﻿using NUnit.Framework;
using Shouldly;
using Zombus.Infrastructure;
using Zombus.UnitTests.TestAssemblies.Handlers;

namespace Zombus.UnitTests.AssemblyScanningTests
{
    [TestFixture]
    public class WhenAHandlerReferencesAMessageThatIsInAnAssemblyThatIsNotIncluded
    {
        [Test]
        public void ValidationShouldFail()
        {
            var assemblyScanningTypeProvider = new AssemblyScanningTypeProvider(typeof (CommandWhoseAssemblyShouldNotBeIncludedHandler).Assembly);
            assemblyScanningTypeProvider.Validate().ShouldNotBeEmpty();
        }
    }
}