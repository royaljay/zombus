﻿using Microsoft.ServiceBus.Messaging;
using Zombus.DependencyResolution;
using Zombus.Interceptors.Inbound;

namespace Zombus.Tests.Common
{
    public class NullInboundInterceptorFactory : IInboundInterceptorFactory
    {
        public IInboundInterceptor[] CreateInterceptors(IDependencyResolverScope scope, object handler, object message, BrokeredMessage brokeredMessage)
        {
            return new IInboundInterceptor[0];
        }
    }
}