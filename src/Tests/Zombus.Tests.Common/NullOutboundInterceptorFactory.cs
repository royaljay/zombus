using Microsoft.ServiceBus.Messaging;
using Zombus.DependencyResolution;
using Zombus.Interceptors.Outbound;

namespace Zombus.Tests.Common
{
    public class NullOutboundInterceptorFactory : IOutboundInterceptorFactory
    {
        public IOutboundInterceptor[] CreateInterceptors(IDependencyResolverScope scope, BrokeredMessage message)
        {
            return new IOutboundInterceptor[0];
        }
    }
}