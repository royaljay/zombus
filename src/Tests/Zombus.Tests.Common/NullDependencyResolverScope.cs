using System;
using Zombus.DependencyResolution;

namespace Zombus.Tests.Common
{
    internal class NullDependencyResolverScope : IDependencyResolverScope
    {
        public IDependencyResolverScope CreateChildScope()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
        }

        public TComponent Resolve<TComponent>()
        {
            throw new NotImplementedException();
        }

        public object Resolve(Type componentType)
        {
            throw new NotImplementedException();
        }
    }
}