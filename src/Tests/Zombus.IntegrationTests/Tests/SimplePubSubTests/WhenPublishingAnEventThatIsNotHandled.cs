﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;
using Shouldly;
using Zombus.IntegrationTests.Tests.SimplePubSubTests.MessageContracts;

namespace Zombus.IntegrationTests.Tests.SimplePubSubTests
{
    public class WhenPublishingAnEventThatIsNotHandled : TestForBus
    {
        private Exception _exception;

        protected override async Task When()
        {
            try
            {
                var myEvent = new SomeEventWeDoNotHandle();
                await Bus.PublishAsync(myEvent);
            }
            catch (Exception exc)
            {
                _exception = exc;
            }
        }

        [Test]
        public async Task NoExceptionIsThrown()
        {
            _exception.ShouldBe(null);
        }
    }
}