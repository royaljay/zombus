﻿using Zombus.MessageContracts;

namespace Zombus.IntegrationTests.Tests.SimplePubSubTests.MessageContracts
{
    public class SomeEventWeOnlyHandleViaCompetition : IBusEvent
    {
    }
}