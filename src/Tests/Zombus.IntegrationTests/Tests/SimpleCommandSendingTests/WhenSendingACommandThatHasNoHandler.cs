﻿using System.Threading.Tasks;
using NUnit.Framework;
using Zombus.IntegrationTests.Tests.SimpleCommandSendingTests.MessageContracts;

namespace Zombus.IntegrationTests.Tests.SimpleCommandSendingTests
{
    [TestFixture]
    public class WhenSendingACommandThatHasNoHandler : TestForBus
    {
        protected override async Task When()
        {
            var someCommand = new SomeCommandThatHasNoHandler();
            await Bus.SendAsync(someCommand);
        }

        [Test]
        public async Task NothingShouldGoBang()
        {
        }
    }
}