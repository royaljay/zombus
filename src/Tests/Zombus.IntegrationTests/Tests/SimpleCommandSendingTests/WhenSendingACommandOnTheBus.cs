﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Zombus.IntegrationTests.Extensions;
using NUnit.Framework;
using Shouldly;
using Zombus.IntegrationTests.Tests.SimpleCommandSendingTests.MessageContracts;
using Zombus.Tests.Common;

namespace Zombus.IntegrationTests.Tests.SimpleCommandSendingTests
{
    [TestFixture]
    [Timeout(_timeoutSeconds * 1000)]
    public class WhenSendingACommandOnTheBus : TestForBus
    {
        private const int _timeoutSeconds = 5;
        protected override async Task When()
        {
            var someCommand = new SomeCommand();
            await Bus.SendAsync(someCommand);
            await TimeSpan.FromSeconds(_timeoutSeconds).WaitUntil(() => MethodCallCounter.AllReceivedMessages.Any());
        }

        [Test]
        public async Task TheCommandBrokerShouldReceiveThatCommand()
        {
            MethodCallCounter.AllReceivedMessages.OfType<SomeCommand>().Count().ShouldBe(1);
        }

        [Test]
        public async Task TheCorrectNumberOfTotalMessagesShouldHaveBeenObserved()
        {
            MethodCallCounter.AllReceivedMessages.Count().ShouldBe(1);
        }
    }
}