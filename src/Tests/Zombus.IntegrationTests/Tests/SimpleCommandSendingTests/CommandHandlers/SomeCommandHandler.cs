﻿using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.IntegrationTests.Tests.SimpleCommandSendingTests.MessageContracts;
using Zombus.Tests.Common;

#pragma warning disable 4014

namespace Zombus.IntegrationTests.Tests.SimpleCommandSendingTests.CommandHandlers
{
    public class SomeCommandHandler : IHandleCommand<SomeCommand>
    {
        public async Task Handle(SomeCommand busCommand)
        {
            MethodCallCounter.RecordCall<SomeCommandHandler>(ch => ch.Handle(busCommand));
        }
    }
}