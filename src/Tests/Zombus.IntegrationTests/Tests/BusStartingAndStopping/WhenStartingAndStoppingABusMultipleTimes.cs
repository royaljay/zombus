﻿using System.Threading.Tasks;
using NUnit.Framework;

namespace Zombus.IntegrationTests.Tests.BusStartingAndStopping
{
    [TestFixture]
    [Ignore("Becuase we respond to events not for receive, we currently do not stop")]
    public class WhenStartingAndStoppingABusMultipleTimes : TestForBus
    {
        protected override async Task When()
        {
            //await Bus.Stop();
            //await Bus.Start();
            //await Bus.Stop();
            //await Bus.Start();
            //await Bus.Stop();
        }

        [Test]
        [Ignore]
        public async Task NothingShouldGoBang()
        {
        }
    }
}