using System;
using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.IntegrationTests.Tests.BusStartingAndStopping.MessageContracts;
using Zombus.Tests.Common;

namespace Zombus.IntegrationTests.Tests.BusStartingAndStopping.Handlers
{
    public class SlowCommandHandler : IHandleCommand<SlowCommand>
    {
        public async Task Handle(SlowCommand busCommand)
        {
            MethodCallCounter.RecordCall<SlowCommandHandler>(h => h.Handle(busCommand));

            await Task.Delay(TimeSpan.FromMilliseconds(500));
        }
    }
}