using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.IntegrationTests.Tests.BusStartingAndStopping.MessageContracts;

namespace Zombus.IntegrationTests.Tests.BusStartingAndStopping.Handlers
{
    public class QuickCommandHandler : IHandleCommand<QuickCommand>
    {
        public async Task Handle(QuickCommand busCommand)
        {
        }
    }
}