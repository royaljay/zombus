﻿using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.IntegrationTests.Tests.AbstractBaseTypeMessageTests.MessageContracts;
using Zombus.Tests.Common;

namespace Zombus.IntegrationTests.Tests.AbstractBaseTypeMessageTests.Handlers
{
    public class SomeConcreteCommandTypeHandler : IHandleCommand<SomeConcreteCommandType>
    {
        public async Task Handle(SomeConcreteCommandType busCommand)
        {
            MethodCallCounter.RecordCall<SomeConcreteCommandTypeHandler>(ch => ch.Handle(busCommand));
        }
    }
}