using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.IntegrationTests.Tests.AbstractBaseTypeMessageTests.MessageContracts;
using Zombus.Tests.Common;

namespace Zombus.IntegrationTests.Tests.AbstractBaseTypeMessageTests.Handlers
{
    public class SomeConcreteEventTypeCompetingHandler : IHandleCompetingEvent<SomeConcreteEventType>
    {
        public async Task Handle(SomeConcreteEventType busEvent)
        {
            MethodCallCounter.RecordCall<SomeConcreteEventTypeCompetingHandler>(ch => ch.Handle(busEvent));
        }
    }
}