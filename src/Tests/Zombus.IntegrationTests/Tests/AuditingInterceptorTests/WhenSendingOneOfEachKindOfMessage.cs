﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Zombus.Infrastructure;
using NUnit.Framework;
using Shouldly;
using Zombus.Configuration;
using Zombus.Infrastructure.DependencyResolution;
using Zombus.IntegrationTests.Tests.AuditingInterceptorTests.MessageTypes;
using Zombus.Interceptors;
using Zombus.MessageContracts.ControlMessages;
using Zombus.Tests.Common;

namespace Zombus.IntegrationTests.Tests.AuditingInterceptorTests
{
    [TestFixture]
    [Timeout(_timeoutSeconds * 1000)]
    public class WhenSendingOneOfEachKindOfMessage : SpecificationForAsync<IBus>
    {
        private const int _timeoutSeconds = 5;

        private object[] _allAuditedMessages;

        protected override async Task<IBus> Given()
        {
            MethodCallCounter.Clear();

            var testFixtureType = GetType();
            var outboundAuditingInterceptorType = typeof (OutboundAuditingInterceptor);
            var auditEventType = typeof (AuditEvent);
            var typeProvider = new TestHarnessTypeProvider(new[] {testFixtureType.Assembly, outboundAuditingInterceptorType.Assembly},
                                                           new[] {testFixtureType.Namespace, outboundAuditingInterceptorType.Namespace, auditEventType.Namespace});
            var logger = TestHarnessLoggerFactory.Create();

            var dependencyResolver = new DependencyResolver(typeProvider);

            var bus = new BusBuilder().Configure()
                                      .WithNames("MyTestSuite", Environment.MachineName)
                                      .WithConnectionString(CommonResources.ServiceBusConnectionString)
                                      .WithTypesFrom(typeProvider)
                                      .WithDependencyResolver(dependencyResolver)
                                      .WithDefaultTimeout(TimeSpan.FromSeconds(10))
                                      .WithMaxDeliveryAttempts(1)
                                      .WithHeartbeatInterval(TimeSpan.MaxValue)
                                      .WithGlobalOutboundInterceptorTypes(typeof (OutboundAuditingInterceptor))
                                      .WithLogger(logger)
                                      .WithDebugOptions(
                                          dc =>
                                          dc.RemoveAllExistingNamespaceElementsOnStartup(
                                              "I understand this will delete EVERYTHING in my namespace. I promise to only use this for test suites."))
                                      .Build();

            await bus.Start();

            return bus;
        }

        protected override async Task When()
        {
            await Task.WhenAll(
                Subject.SendAsync(new SomeCommand(42)),
                Subject.SendAtAsync(new SomeCommandSentViaDelay(), DateTimeOffset.UtcNow),
                Subject.PublishAsync(new SomeEvent())
                );

            await TimeSpan.FromSeconds(_timeoutSeconds).WaitUntil(() => MethodCallCounter.AllReceivedMessages.Count() >= 3);
            MethodCallCounter.Stop();

            _allAuditedMessages = MethodCallCounter.AllReceivedMessages
                                                   .OfType<AuditEvent>()
                                                   .Select(ae => ae.MessageBody)
                                                   .ToArray();
        }

        [Test]
        public async Task ThereShouldBeAnAuditRecordForSomeCommand()
        {
            _allAuditedMessages.OfType<SomeCommand>().Count().ShouldBe(1);
        }

        [Test]
        public async Task ThereShouldBeAnAuditRecordForSomeCommandSentViaDelay()
        {
            _allAuditedMessages.OfType<SomeCommandSentViaDelay>().Count().ShouldBe(1);
        }

        [Test]
        public async Task ThereShouldBeAnAuditRecordForSomeEvent()
        {
            _allAuditedMessages.OfType<SomeEvent>().Count().ShouldBe(1);
        }

        [Test]
        public async Task ThereShouldBeATotalOfThreeAuditRecords()
        {
            MethodCallCounter.AllReceivedMessages.OfType<AuditEvent>().Count().ShouldBe(3);
        }

        [Test]
        public async Task ThereShouldBeATotalOfThreeRecordedHandlerCalls()
        {
            MethodCallCounter.AllReceivedCalls.Count().ShouldBe(3);
        }
    }
}