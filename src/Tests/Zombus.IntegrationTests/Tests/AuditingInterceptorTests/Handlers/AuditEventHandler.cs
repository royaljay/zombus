﻿using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.MessageContracts.ControlMessages;
using Zombus.Tests.Common;

namespace Zombus.IntegrationTests.Tests.AuditingInterceptorTests.Handlers
{
    public class AuditEventHandler : IHandleCompetingEvent<AuditEvent>
    {
        public async Task Handle(AuditEvent busEvent)
        {
            MethodCallCounter.RecordCall<AuditEventHandler>(h => h.Handle(busEvent));
            TestHarnessLoggerFactory.Create().Debug("Received audit message {@AuditMessage}", busEvent);
        }
    }
}