﻿using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.IntegrationTests.Tests.SimpleDispatchContextCorrelationTests.MessageContracts;
using Zombus.PropertyInjection;
using Zombus.Tests.Common;

namespace Zombus.IntegrationTests.Tests.SimpleDispatchContextCorrelationTests.CommandHandlers
{
    public class FirstCommandHandler : IHandleCommand<FirstCommand>, IRequireBus
    {
        public IBus Bus { get; set; }
        public IDispatchContext DispatchContext { get; set; }

        public async Task Handle(FirstCommand busCommand)
        {
            MethodCallCounter.RecordCall<FirstCommandHandler>(ch => ch.Handle(busCommand));

            await Bus.SendAsync(new SecondCommand());
        }
    }
}