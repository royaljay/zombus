﻿using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.IntegrationTests.Tests.SimpleDispatchContextCorrelationTests.MessageContracts;
using Zombus.PropertyInjection;
using Zombus.Tests.Common;

namespace Zombus.IntegrationTests.Tests.SimpleDispatchContextCorrelationTests.CommandHandlers
{
    public class SecondCommandHandler : IHandleCommand<SecondCommand>, IRequireBus
    {
        public IBus Bus { get; set; }

        public async Task Handle(SecondCommand busCommand)
        {
            MethodCallCounter.RecordCall<SecondCommandHandler>(ch => ch.Handle(busCommand));
            await Bus.SendAsync(new ThirdCommand());
        }
    }
}