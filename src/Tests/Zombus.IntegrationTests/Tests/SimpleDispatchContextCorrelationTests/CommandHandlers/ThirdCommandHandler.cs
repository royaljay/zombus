﻿using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.IntegrationTests.Tests.SimpleDispatchContextCorrelationTests.MessageContracts;
using Zombus.Tests.Common;

namespace Zombus.IntegrationTests.Tests.SimpleDispatchContextCorrelationTests.CommandHandlers
{
    public class ThirdCommandHandler : IHandleCommand<ThirdCommand>
    {
        public async Task Handle(ThirdCommand busCommand)
        {
            MethodCallCounter.RecordCall<ThirdCommandHandler>(ch => ch.Handle(busCommand));
        }
    }
}