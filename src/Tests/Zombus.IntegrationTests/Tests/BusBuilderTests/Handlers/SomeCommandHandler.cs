﻿using System;
using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.IntegrationTests.Tests.BusBuilderTests.MessageContracts;

namespace Zombus.IntegrationTests.Tests.BusBuilderTests.Handlers
{
    public class SomeCommandHandler : IHandleCommand<SomeCommand>
    {
        public Task Handle(SomeCommand busCommand)
        {
            throw new NotImplementedException();
        }
    }
}