﻿using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.IntegrationTests.Tests.InterceptorTests.Interceptors;
using Zombus.IntegrationTests.Tests.InterceptorTests.MessageContracts;
using Zombus.Interceptors.Inbound;

namespace Zombus.IntegrationTests.Tests.InterceptorTests.Handlers
{
    [Interceptor(typeof (SomeBaseClassLevelInterceptor))]
    public abstract class SomeBaseCommandHandler : IHandleCommand<FooCommand>, IHandleCommand<BarCommand>
    {
        [Interceptor(typeof (SomeBaseMethodLevelInterceptorForFoo))]
        public abstract Task Handle(FooCommand busCommand);

        [Interceptor(typeof (SomeBaseMethodLevelInterceptorForBar))]
        public abstract Task Handle(BarCommand busCommand);
    }
}