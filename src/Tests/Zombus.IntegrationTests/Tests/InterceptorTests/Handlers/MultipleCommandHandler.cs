﻿using System.Threading.Tasks;
using Zombus.IntegrationTests.Tests.InterceptorTests.Interceptors;
using Zombus.IntegrationTests.Tests.InterceptorTests.MessageContracts;
using Zombus.Interceptors.Inbound;
using Zombus.Tests.Common;

namespace Zombus.IntegrationTests.Tests.InterceptorTests.Handlers
{
    [Interceptor(typeof (SomeClassLevelInterceptor))]
    public class MultipleCommandHandler : SomeBaseCommandHandler
    {
        [Interceptor(typeof (SomeMethodLevelInterceptorForFoo))]
        public override async Task Handle(FooCommand busCommand)
        {
            MethodCallCounter.RecordCall<MultipleCommandHandler>(h => h.Handle(busCommand));
        }

        [Interceptor(typeof (SomeMethodLevelInterceptorForBar))]
        public override async Task Handle(BarCommand busCommand)
        {
            MethodCallCounter.RecordCall<MultipleCommandHandler>(h => h.Handle(busCommand));
        }
    }
}