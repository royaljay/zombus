﻿using System;
using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.IntegrationTests.Tests.PoisonMessageTests.MessageContracts;
using Zombus.Tests.Common;

namespace Zombus.IntegrationTests.Tests.PoisonMessageTests.CommandHandlers
{
    public class GoBangCommandHandler : IHandleCommand<GoBangCommand>
    {
        public async Task Handle(GoBangCommand busCommand)
        {
            MethodCallCounter.RecordCall<GoBangCommandHandler>(h => h.Handle(busCommand));

            throw new Exception("This handler is supposed to fail.");
        }
    }
}