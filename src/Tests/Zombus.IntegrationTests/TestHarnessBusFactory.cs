using System;
using System.Linq;
using System.Threading.Tasks;
using Zombus.Configuration;
using Zombus.Infrastructure.DependencyResolution;
using Zombus.Interceptors.Inbound;
using Zombus.Interceptors.Outbound;
using Zombus.Tests.Common;

namespace Zombus.IntegrationTests
{
    public class TestHarnessBusFactory
    {
        private readonly Type _testFixtureType;

        public TestHarnessBusFactory(Type testFixtureType)
        {
            _testFixtureType = testFixtureType;
        }

        public async Task<Bus> CreateAndStart()
        {
            var logger = TestHarnessLoggerFactory.Create();
            //var logger = new NullLogger();

            // Filter types we care about to only our own test's namespace. It's a performance optimisation because creating and
            // deleting queues and topics is slow.
            var typeProvider = new TestHarnessTypeProvider(new[] {_testFixtureType.Assembly}, new[] {_testFixtureType.Namespace});

            var bus = new BusBuilder().Configure()
                                      .WithNames("MyTestSuite", Environment.MachineName)
                                      .WithConnectionString(CommonResources.ServiceBusConnectionString)
                                      .WithTypesFrom(typeProvider)
                                      .WithGlobalInboundInterceptorTypes(typeProvider.InterceptorTypes.Where(t => typeof (IInboundInterceptor).IsAssignableFrom(t)).ToArray())
                                      .WithGlobalOutboundInterceptorTypes(typeProvider.InterceptorTypes.Where(t => typeof (IOutboundInterceptor).IsAssignableFrom(t)).ToArray())
                                      .WithDependencyResolver(new DependencyResolver(typeProvider))
                                      .WithDefaultTimeout(TimeSpan.FromSeconds(10))
                                      .WithHeartbeatInterval(TimeSpan.MaxValue)
                                      .WithLogger(logger)
                                      .WithDebugOptions(
                                          dc =>
                                          dc.RemoveAllExistingNamespaceElementsOnStartup(
                                              "I understand this will delete EVERYTHING in my namespace. I promise to only use this for test suites."))
                                      .Build();
            await bus.Start(MessageReceiverTypes.All);

            return bus;
        }
    }
}