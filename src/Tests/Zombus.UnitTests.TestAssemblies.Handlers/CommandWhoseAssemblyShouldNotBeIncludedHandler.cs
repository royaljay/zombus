﻿using System;
using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.UnitTests.TestAssemblies.MessageContracts;

namespace Zombus.UnitTests.TestAssemblies.Handlers
{
    public class CommandWhoseAssemblyShouldNotBeIncludedHandler : IHandleCommand<CommandWhoseAssemblyShouldNotBeIncluded>
    {
        public async Task Handle(CommandWhoseAssemblyShouldNotBeIncluded busCommand)
        {
            throw new NotImplementedException();
        }
    }
}