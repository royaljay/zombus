using System.Linq;
using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.PropertyInjection;
using Zombus.StressTests.ThreadStarvationTests.Cascades.MessageContracts;

namespace Zombus.StressTests.ThreadStarvationTests.Cascades.Handlers
{
    public class ThingBHappenedEventHandler : IHandleCompetingEvent<ThingBHappenedEvent>, IRequireBus, ILongRunningTask
    {
        public const int NumberOfDoThingCCommands = 11;

        public IBus Bus { get; set; }

        public async Task Handle(ThingBHappenedEvent busEvent)
        {
            var commands = Enumerable.Range(0, NumberOfDoThingCCommands)
                                     .Select(i => new DoThingCCommand())
                                     .ToArray();

            await Bus.SendAll(commands);
        }

        public bool IsAlive
        {
            get { return true; }
        }
    }
}