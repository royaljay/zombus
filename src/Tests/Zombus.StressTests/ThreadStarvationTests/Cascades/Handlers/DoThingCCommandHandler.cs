using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.StressTests.ThreadStarvationTests.Cascades.MessageContracts;
using Zombus.Tests.Common;

namespace Zombus.StressTests.ThreadStarvationTests.Cascades.Handlers
{
    public class DoThingCCommandHandler : IHandleCommand<DoThingCCommand>
    {
        public async Task Handle(DoThingCCommand busCommand)
        {
            MethodCallCounter.RecordCall<DoThingCCommandHandler>(h => h.Handle(busCommand));
        }
    }
}