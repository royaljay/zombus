﻿using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.PropertyInjection;
using Zombus.StressTests.ThreadStarvationTests.Cascades.MessageContracts;

namespace Zombus.StressTests.ThreadStarvationTests.Cascades.Handlers
{
    public class DoThingBCommandHandler : IHandleCommand<DoThingBCommand>, IRequireBus
    {
        public IBus Bus { get; set; }

        public async Task Handle(DoThingBCommand busCommand)
        {
            await Bus.PublishAsync(new ThingBHappenedEvent());
        }
    }
}