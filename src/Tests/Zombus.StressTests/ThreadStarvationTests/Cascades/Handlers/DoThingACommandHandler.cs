﻿using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.PropertyInjection;
using Zombus.StressTests.ThreadStarvationTests.Cascades.MessageContracts;

namespace Zombus.StressTests.ThreadStarvationTests.Cascades.Handlers
{
    public class DoThingACommandHandler : IHandleCommand<DoThingACommand>, IRequireBus
    {
        public IBus Bus { get; set; }

        public async Task Handle(DoThingACommand busCommand)
        {
            await Bus.PublishAsync(new ThingAHappenedEvent());
        }
    }
}