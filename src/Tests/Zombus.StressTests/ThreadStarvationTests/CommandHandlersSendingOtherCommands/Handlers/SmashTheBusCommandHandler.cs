﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.PropertyInjection;
using Zombus.StressTests.ThreadStarvationTests.CommandHandlersSendingOtherCommands.MessageContracts;

namespace Zombus.StressTests.ThreadStarvationTests.CommandHandlersSendingOtherCommands.Handlers
{
    public class SmashTheBusCommandHandler: IHandleCommand<SmashTheBusCommand>, ILongRunningTask, IRequireBus
    {
        public static int NumCommandsSent = 0;
        public static TimeSpan HammerTheBusFor = TimeSpan.FromSeconds(20);

        public IBus Bus { get; set; }

        public async Task Handle(SmashTheBusCommand busCommand)
        {
            const int batchSize = 10;

            var sw = Stopwatch.StartNew();
            while (sw.Elapsed < HammerTheBusFor)
            {
                var commands = Enumerable.Range(0, batchSize)
                    .Select(i => new NoOpCommand())
                    .ToArray();
                await Bus.SendAll(commands);
                NumCommandsSent += batchSize;
            }
        }

        public bool IsAlive { get { return true; } }
    }
}