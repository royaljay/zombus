﻿using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.StressTests.ThreadStarvationTests.CommandHandlersSendingOtherCommands.MessageContracts;
using Zombus.Tests.Common;

namespace Zombus.StressTests.ThreadStarvationTests.CommandHandlersSendingOtherCommands.Handlers
{
    public class NoOpCommandHandler: IHandleCommand<NoOpCommand>
    {
        public async Task Handle(NoOpCommand busCommand)
        {
            MethodCallCounter.RecordCall<NoOpCommandHandler>(h => h.Handle(busCommand));
        }
    }
}