﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Shouldly;
using Zombus.Configuration;
using Zombus.Infrastructure.DependencyResolution;
using Zombus.Interceptors.Inbound;
using Zombus.Interceptors.Outbound;
using Zombus.StressTests.ThreadStarvationTests.CommandHandlersSendingOtherCommands.Handlers;
using Zombus.StressTests.ThreadStarvationTests.CommandHandlersSendingOtherCommands.MessageContracts;
using Zombus.Tests.Common;

namespace Zombus.StressTests.ThreadStarvationTests.CommandHandlersSendingOtherCommands
{
    internal class WhenACommandHandlerSmashesTheBusForMoreThanItsLockDuration : SpecificationForAsync<Bus>
    {
        private const int _timeoutSeconds = 180;
        private static readonly TimeSpan _messageLockDuration = TimeSpan.FromSeconds(20);

        protected override async Task<Bus> Given()
        {
            var logger = TestHarnessLoggerFactory.Create();

            var typeProvider = new TestHarnessTypeProvider(new[] {GetType().Assembly}, new[] {GetType().Namespace});

            var bus = new BusBuilder().Configure()
                                      .WithNames("MyTestSuite", Environment.MachineName)
                                      .WithConnectionString(CommonResources.ServiceBusConnectionString)
                                      .WithTypesFrom(typeProvider)
                                      .WithGlobalInboundInterceptorTypes(typeProvider.InterceptorTypes.Where(t => typeof (IInboundInterceptor).IsAssignableFrom(t)).ToArray())
                                      .WithGlobalOutboundInterceptorTypes(typeProvider.InterceptorTypes.Where(t => typeof (IOutboundInterceptor).IsAssignableFrom(t)).ToArray())
                                      .WithDependencyResolver(new DependencyResolver(typeProvider))
                                      .WithDefaultTimeout(TimeSpan.FromSeconds(10))
                                      .WithDefaultMessageLockDuration(_messageLockDuration)
                                      .WithLogger(logger)
                                      .WithDebugOptions(
                                          dc =>
                                          dc.RemoveAllExistingNamespaceElementsOnStartup(
                                              "I understand this will delete EVERYTHING in my namespace. I promise to only use this for test suites."))
                                      .Build();
            await bus.Start();

            return bus;
        }

        protected override async Task When()
        {
            SmashTheBusCommandHandler.NumCommandsSent = 0;
            await Subject.SendAsync(new SmashTheBusCommand());
            await Task.Delay(SmashTheBusCommandHandler.HammerTheBusFor);

            var remainingTime = TimeSpan.FromSeconds(_timeoutSeconds) - SmashTheBusCommandHandler.HammerTheBusFor;
            await remainingTime.WaitUntil(() => MethodCallCounter.AllReceivedMessages.OfType<NoOpCommand>().Count() >= SmashTheBusCommandHandler.NumCommandsSent);
        }

        [Test]
        public async Task NoMessagesShouldHaveLocksExpire()
        {
            MethodCallCounter.AllReceivedMessages.OfType<NoOpCommand>().Count().ShouldBe(SmashTheBusCommandHandler.NumCommandsSent);
        }
    }
}