﻿using Zombus.MessageContracts;

namespace Zombus.StressTests.ThreadStarvationTests.CommandHandlersSendingOtherCommands.MessageContracts
{
    public class NoOpCommand : IBusCommand
    {
    }
}