﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Zombus.Handlers;
using Zombus.StressTests.ThreadStarvationTests.BadlyBehavedHandlersThatDoNotKnowAboutAsync.MessageContracts;
using Zombus.Tests.Common;

namespace Zombus.StressTests.ThreadStarvationTests.BadlyBehavedHandlersThatDoNotKnowAboutAsync.Handlers
{
    public class CommandThatWillBlockTheThreadHandler : IHandleCommand<CommandThatWillBlockTheThread>, ILongRunningTask
    {
        public static readonly TimeSpan SleepDuration = TimeSpan.FromSeconds(10);

        public async Task Handle(CommandThatWillBlockTheThread busCommand)
        {
            Thread.Sleep(SleepDuration); // deliberately block the handling thread
            MethodCallCounter.RecordCall<CommandThatWillBlockTheThreadHandler>(h => h.Handle(busCommand));
        }

        public bool IsAlive
        {
            get { return true; }
        }
    }
}