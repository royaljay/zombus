﻿using Zombus.MessageContracts;

namespace Zombus.StressTests.ThreadStarvationTests.BadlyBehavedHandlersThatDoNotKnowAboutAsync.MessageContracts
{
    public class CommandThatWillBlockTheThread : IBusCommand
    {
    }
}