﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Zombus.StressTests.ThroughputTests.MessageContracts;

namespace Zombus.StressTests.ThroughputTests
{
    [TestFixture]
    public class WhenSendingManyLargeCommandsOfTheSameType : ThroughputSpecificationForBus
    {
        protected override int NumMessagesToSend
        {
            get { return 1000; }
        }

        protected override int ExpectedMessagesPerSecond
        {
            get { return 50; }
        }

        public override IEnumerable<Task> SendMessages(IBus bus)
        {
            for (var i = 0; i < NumMessagesToSend; i++)
            {
                var command = new FooCommand
                              {
                                  SomeMessage = new string(Enumerable.Range(0, 32*1024).Select(j => '.').ToArray()),
                              };
                yield return bus.SendAsync(command);
                Console.Write(".");
            }
            Console.WriteLine();
        }
    }
}