﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NUnit.Framework;
using Zombus.StressTests.ThroughputTests.MessageContracts;

namespace Zombus.StressTests.ThroughputTests
{
    [TestFixture]
    public class WhenSendingManyCommandsOfDifferentTypes : ThroughputSpecificationForBus
    {
        protected override int ExpectedMessagesPerSecond
        {
            get { return 150; }
        }

        public override IEnumerable<Task> SendMessages(IBus bus)
        {
            for (var i = 0; i < NumMessagesToSend/4; i++)
            {
                yield return bus.SendAsync(new FooCommand());
                yield return bus.SendAsync(new BarCommand());
                yield return bus.SendAsync(new BazCommand());
                yield return bus.SendAsync(new QuxCommand());
                Console.Write(".");
            }
            Console.WriteLine();
        }
    }
}