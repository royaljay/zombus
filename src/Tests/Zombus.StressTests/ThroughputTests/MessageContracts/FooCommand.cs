﻿using Zombus.MessageContracts;

namespace Zombus.StressTests.ThroughputTests.MessageContracts
{
    public class FooCommand : IBusCommand
    {
        public string SomeMessage { get; set; }
    }
}