# Zombus
Zombus is a .NET library forked off Nimbus. It makes working with Azure service bus easier.

**Zombus adds support for**

* Azure Service Bus 2.7
* ASB Client OnMessage received (built in message pump and thread pool)


**Removes**

* Custom Threadpool & Thread scheduler
* Message pumps
* Request response support


